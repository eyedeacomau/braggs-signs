<?php
    defined('C5_EXECUTE') or die("Access Denied.");
    $edit = false;
    if($c->isEditMode()) $edit = true;
    $u = new User();
    $home = false;
    // if(substr_count($c->cPath,'/') == 1) $home = true;  // Multilingual
    if(is_null($c->cPath)) $home = true;
?>
<!DOCTYPE html>
<html lang="<?php echo Localization::activeLanguage()?>">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <?php if(false): ?>
        <link rel="stylesheet" type="text/css" href="<?php echo $view->getThemePath()?>/css/bootstrap-modified.css">
        <?php //echo $html->css($view->getStylesheet('main.less'))?>
    <?php endif; ?>
    <?php Loader::element('header_required', array('pageTitle' => isset($pageTitle) ? $pageTitle : '', 'pageDescription' => isset($pageDescription) ? $pageDescription : '', 'pageMetaKeywords' => isset($pageMetaKeywords) ? $pageMetaKeywords : ''));?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script>
        if (navigator.userAgent.match(/IEMobile\/10\.0/)) {
            var msViewportStyle = document.createElement('style')
            msViewportStyle.appendChild(
                document.createTextNode(
                    '@-ms-viewport{width:auto!important}'
                )
            )
            document.querySelector('head').appendChild(msViewportStyle)
        }
    </script>
    <link rel="stylesheet" type="text/css" href="<?=$view->getThemePath(); ?>/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$view->getThemePath(); ?>/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$view->getThemePath(); ?>/css/main.css">
    <link href="//fonts.googleapis.com/css?family=Armata:400,400i" rel="stylesheet">
    <?php if(!$u->isLoggedIn()): ?>
        <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
        <script type="text/javascript" src="<?=$view->getThemePath(); ?>/js/bootstrap.min.js"></script>
    <?php else: ?>

    <?php endif; ?>
    <script type="text/javascript" src="<?=$view->getThemePath(); ?>/js/grids.min.js"></script>
    <script type="text/javascript" src="<?=$view->getThemePath(); ?>/js/main.js"></script>
</head>
<body class="<?=($home ? 'home ' : '');?><?=($edit ? 'edit ' : '');?>">
<div class="<?=$c->getPageWrapperClass()?>">

<header id="hd">
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" id="menu-toggle" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/"><img src="<?=$view->getThemePath(); ?>/images/logo.png" alt="Braggs Signs" /></a>
            </div>

            <div id="navbar" class="navbar-collapse collapse">
                <?php
                    $a = new GlobalArea('Menu');
                    $a->display();
                ?>
                <?php if($edit): ?>
                    <div class="row"><div class="col-xs-12"><br />
                <?php else: ?>
                    <ul class="nav navbar-nav navbar-right">
                <?php endif; ?>
                    <?php
                    $a = new GlobalArea('Header Info');
                    $a->display();
                    ?>
                <?php if($edit): ?>
                    </div></div>
                <?php else: ?>
                    </ul>
                <?php endif; ?>
            </div>
        </div>
    </nav>
</header>
