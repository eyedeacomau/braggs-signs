<?php defined('C5_EXECUTE') or die("Access Denied.");

?>

<footer id="ft">
    <div id="contact-header">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <?php
                    $a = new GlobalArea('Contact Header');
                    $a->display();
                    ?>
                </div>
            </div>
        </div>
    </div>
    <?php if($c->cPath != '/contact'): ?>
    <div id="contact-map">
        <?php
        $a = new GlobalArea('Contact Map');
        $a->display();
        ?>
    </div>
    <?php endif; ?>
    <div id="contact">
        <div class="container">
            <div class="row">
                <?php
                $a = new GlobalArea('Footer Contact');
                $a->display();
                ?>
            </div>
        </div>
    </div>
    <div class="footer-bottom container-fluid">
        <div class="row">
            <div class="col-sm-4 col-md-3">
                <a class="navbar-brand" href="/"><img src="<?=$view->getThemePath(); ?>/images/logo.png" alt="Braggs Signs" /></a>
            </div>
            <div class="col-sm-6 col-md-6 middle">
            <?php
            $a = new GlobalArea('Footer Middle');
            $a->display();
            ?>
            </div>
            <div class="col-sm-2 col-md-3">
                <a id="back-to-top" href="#top"><i class="fa fa-chevron-up"></i><span class="sr-only">Back to top</span></a>
            </div>
        </div>
    </div>
</footer>
</div>
<?php Loader::element('footer_required'); ?>
</body>
</html>

