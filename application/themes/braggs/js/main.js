(function($) {
	$(document).ready(function() {

        if($('#map').length) {
            var style = [
			    {
			        "featureType": "administrative.land_parcel",
			        "elementType": "labels",
			        "stylers": [
			            {
			                "visibility": "off"
			            }
			        ]
			    },
			    {
			        "featureType": "landscape.man_made",
			        "elementType": "all",
			        "stylers": [
			            {
			                "visibility": "simplified"
			            }
			        ]
			    },
			    {
			        "featureType": "landscape.natural",
			        "elementType": "all",
			        "stylers": [
			            {
			                "visibility": "simplified"
			            }
			        ]
			    },
			    {
			        "featureType": "poi",
			        "elementType": "all",
			        "stylers": [
			            {
			                "visibility": "off"
			            }
			        ]
			    },
			    {
			        "featureType": "transit",
			        "elementType": "all",
			        "stylers": [
			            {
			                "visibility": "off"
			            }
			        ]
			    },
			    {
			        "featureType": "water",
			        "elementType": "all",
			        "stylers": [
			            {
			                "visibility": "simplified"
			            }
			        ]
			    }
			],
	            zoom = 3
	            lat = -25,
	            lng = 120;
            if($('#map-block').hasClass('full')) {
                zoom = 5;
                lat = -27;
                lng = 130;
            }
            var map = new GMaps({
                div: '#map',
                scrollwheel: false,
                lat: lat,
                lng: lng,
                zoom: zoom,
                disableDefaultUI: false,
                styles: style
            });
            if(markers.length) {
                $.each(markers, function(i, val) {
                    val.lat = floatval(val.lat);
                    val.lng = floatval(val.lng);
                    map.addMarker(val);
                });
            }
            $('#contact-header').hide();
            $('#map-block > .row > div').responsiveEqualHeightGrid();
        }
        if($('.owl-carousel').length) {
            var owl = $('.owl-carousel').owlCarousel({
                loop: true,
                margin: 0,
                nav: false,
                responsive: {
                    0: {
                        items: 1
                    },
                    480: {
                        items: 2
                    },
                    768: {
                        items: 4
                    },
                    992: {
                        items: 6
                    }
                }
            });
            $('.carousel').on('click', '.next', function(e) {
                e.preventDefault();
                owl.trigger('next.owl.carousel');
            }).on('click', '.prev', function(e) {
                e.preventDefault();
                owl.trigger('owl.prev.carousel');
            });
        }
        if($('#steps').length) {
            $('.row.arrows > div', '#steps').responsiveEqualHeightGrid();
        }
        $('#ft').on('click', '#back-to-top', function(e) {
            e.preventDefault();
            $('html,body').animate({
                scrollTop: 0
            }, 700, 'easeOutExpo');
        });

        function updateSlider(slider) {
            if(typeof slider === 'undefined') slider = $('.header-slider').first();
            if(slider.length) {
                var hd = $('nav.navbar-default', '#hd').outerHeight();
                if(slider.prev().length) hd = 0;

                var box_height = slider.outerHeight() - hd;
                var header_element = $('.header-box', slider);
                if(header_element.length) header_element.each(function(i, val) {
                    $(val).css('marginTop', intval((box_height - $(val).outerHeight()) /2 + hd)+'px');
                });
                var nav = $('a.rslides_nav', slider);
                if(nav.length) nav.css('top', intval((box_height - nav.outerHeight()) / 2 + hd)+'px');
            }
        }
        var header_sliders = $('ul.rslides','.header-slider');
        if(header_sliders.length) {
            $.each(header_sliders, function(i, val) {
                var header_slider = $(val);
                if(header_slider.length) {
                    var slideoptions = {
                        prevText: '',   // String: Text for the "previous" button
                        nextText: '',
                        before: updateSlider(header_slider.closest('.header-slider')),
                        nav: true,
                        pager: true
                    },
                    timeout = header_slider.data('timeout'),
                    speed = header_slider.data('speed'),
                    pause = header_slider.data('pause'),
                    noanimate = header_slider.data('noanimate');
                    if(timeout > 0) slideoptions.timeout = timeout;
                    if(speed > 0) slideoptions.speed = speed;
                    if(pause) slideoptions.pause = pause;
                    if(noanimate) slideoptions.auto = false;
                    header_slider.responsiveSlides(slideoptions);
                }
            });
        }
		function updatePositions() {
            var header_sliders = $('.header-slider');
            if(header_sliders.length) $.each(header_sliders, function(i, val) {
                updateSlider($(val));
            });
            var header = $('main > picture:first-child img');
            if(header.length) {
                if(window.innerWidth < 768 && header.width() > window.innerWidth) {
                    header.css('marginLeft', '-'+intval((header.outerWidth() - window.innerWidth) / 2)+'px');
                } else {
                    header.css('marginLeft', 0);
                }
            }
		}
		updatePositions();
		var resize_timer;
		$(window).resize(function() {
			clearTimeout(resize_timer);
			resize_timer = setTimeout(updatePositions, 100);
		});
	});
})(jQuery);

function getHashFilter() {
  var matches = location.hash.match(/filter=([^&]+)/i); // get filter=filterName
  var hashFilter = matches && matches[1];
  return hashFilter && decodeURIComponent(hashFilter);
}
function intval(v) {
    v = parseInt(v);
    return isNaN(v) ? 0 : v;
}
function floatval(v) {
    v = parseFloat(v);
    return isNaN(v) ? 0 : v;
}
function getDigits(v) {
    return v.replace(/[^\d]/g, '');
}
function slug(string) {
    if(typeof string == 'undefined') string = '';
    return string.replace(/(^\-+|[^a-zA-Z0-9\/_| -]+|\-+$)/g, '').toLowerCase().replace(/[\/_| -]+/g, '-');
}
function randomint(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}
Number.prototype.map = function(in_min, in_max, out_min, out_max) {
    return (this - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}
Number.prototype.formatMoney = function(c, d, t) { /* (decimal places, decimal, thousands separator */
    var n = this, c = isNaN(c = Math.abs(c)) ? 2 : c, d = d == undefined ? "." : d, t = t == undefined ? "," : t, s = n < 0 ? "-" : "", i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};
function getMoney(strMoney) {
    return floatval(floatval(jQuery.trim(strMoney.replace(/[^\d\.]/g, ''))).toFixed(2));
}
function shuffle(v) {
    for(var j, x, i = v.length; i; j = parseInt(Math.random() * i), x = v[--i], v[i] = v[j], v[j] = x);
    return v;
}
function capitalize(str) {
    return str.replace(/\w\S*/g, function(txt) {return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
}
jQuery.easing.easeOutExpo = function (x, t, b, c, d) {
	return (t==d) ? b+c : c * (-Math.pow(2, -10 * t/d) + 1) + b;
}
