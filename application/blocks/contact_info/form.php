<?php  defined("C5_EXECUTE") or die("Access Denied."); ?>

<div class="form-group">
    <?php  echo $form->label('heading', t("Heading")); ?>
    <?php  echo isset($btFieldsRequired) && in_array('heading', $btFieldsRequired) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <?php  echo $form->text($view->field('heading'), $heading, array (
  'maxlength' => 255,
)); ?>
</div>

<div class="form-group">
    <?php  echo $form->label('address', t("Address")); ?>
    <?php  echo isset($btFieldsRequired) && in_array('address', $btFieldsRequired) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <?php  echo $form->text($view->field('address'), $address, array (
  'maxlength' => 255,
)); ?>
</div>

<div class="form-group">
    <?php  echo $form->label('postal', t("Postal Address")); ?>
    <?php  echo isset($btFieldsRequired) && in_array('postal', $btFieldsRequired) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <?php  echo $form->text($view->field('postal'), $postal, array (
  'maxlength' => 255,
)); ?>
</div>

<div class="form-group">
    <?php  echo $form->label('email', t("Email Display Text")); ?>
    <?php  echo isset($btFieldsRequired) && in_array('email', $btFieldsRequired) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <?php  echo $form->text($view->field('email'), $email, array (
  'maxlength' => 255,
)); ?>
</div>

<div class="form-group">
    <?php  echo $form->label('emaillink', t("Email Address")); ?>
    <?php  echo isset($btFieldsRequired) && in_array('emaillink', $btFieldsRequired) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <?php  echo $form->text($view->field('emaillink'), $emaillink, array (
  'maxlength' => 255,
  'placeholder' => 'info@braggs.com.au',
)); ?>
</div>

<div class="form-group">
    <?php  echo $form->label('phone', t("Phone Display Text")); ?>
    <?php  echo isset($btFieldsRequired) && in_array('phone', $btFieldsRequired) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <?php  echo $form->text($view->field('phone'), $phone, array (
  'maxlength' => 255,
)); ?>
</div>

<div class="form-group">
    <?php  echo $form->label('phonelink', t("Phone Link")); ?>
    <?php  echo isset($btFieldsRequired) && in_array('phonelink', $btFieldsRequired) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <?php  echo $form->text($view->field('phonelink'), $phonelink, array (
  'maxlength' => 255,
  'placeholder' => 'tel:1800...',
)); ?>
</div>

<div class="form-group">
    <?php
    if (isset($image) && $image > 0) {
        $image_o = File::getByID($image);
        if (!is_object($image_o)) {
            unset($image_o);
        }
    } ?>
    <?php  echo $form->label('image', t("Image")); ?>
    <?php  echo isset($btFieldsRequired) && in_array('image', $btFieldsRequired) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <?php  echo Core::make("helper/concrete/asset_library")->image('ccm-b-contact_info-image-' . Core::make('helper/validation/identifier')->getString(18), $view->field('image'), t("Choose Image"), $image_o); ?>
</div>
