<?php  namespace Application\Block\ContactInfo;

defined("C5_EXECUTE") or die("Access Denied.");

use Concrete\Core\Block\BlockController;
use Core;
use File;
use Page;

class Controller extends BlockController
{
    public $helpers = array('form');
    public $btFieldsRequired = array();
    protected $btExportFileColumns = array('image');
    protected $btTable = 'btContactInfo';
    protected $btInterfaceWidth = 400;
    protected $btInterfaceHeight = 500;
    protected $btIgnorePageThemeGridFrameworkContainer = true;
    protected $btCacheBlockRecord = true;
    protected $btCacheBlockOutput = true;
    protected $btCacheBlockOutputOnPost = true;
    protected $btCacheBlockOutputForRegisteredUsers = true;
    protected $btCacheBlockOutputLifetime = 0;
    protected $pkg = false;

    public function getBlockTypeDescription()
    {
        return t("");
    }

    public function getBlockTypeName()
    {
        return t("Contact Info");
    }

    public function getSearchableContent()
    {
        $content = array();
        $content[] = $this->heading;
        $content[] = $this->address;
        $content[] = $this->postal;
        $content[] = $this->email;
        $content[] = $this->emaillink;
        $content[] = $this->phone;
        $content[] = $this->phonelink;
        return implode(" ", $content);
    }

    public function view()
    {

        if ($this->image && ($f = File::getByID($this->image)) && is_object($f)) {
            $this->set("image", $f);
        } else {
            $this->set("image", false);
        }
    }

    public function add()
    {
        $this->addEdit();
    }

    public function edit()
    {
        $this->addEdit();
    }

    protected function addEdit()
    {
        $this->requireAsset('core/file-manager');
        $this->set('btFieldsRequired', $this->btFieldsRequired);
    }

    public function validate($args)
    {
        $e = Core::make("helper/validation/error");
        if (in_array("heading", $this->btFieldsRequired) && (trim($args["heading"]) == "")) {
            $e->add(t("The %s field is required.", t("Heading")));
        }
        if (in_array("address", $this->btFieldsRequired) && (trim($args["address"]) == "")) {
            $e->add(t("The %s field is required.", t("Address")));
        }
        if (in_array("address", $this->btFieldsRequired) && (trim($args["postal"]) == "")) {
            $e->add(t("The %s field is required.", t("Postal Address")));
        }
        if (in_array("email", $this->btFieldsRequired) && (trim($args["email"]) == "")) {
            $e->add(t("The %s field is required.", t("Email Address")));
        }
        if (in_array("emaillink", $this->btFieldsRequired) && (trim($args["emaillink"]) == "")) {
            $e->add(t("The %s field is required.", t("Email Link Text")));
        }
        if (in_array("phone", $this->btFieldsRequired) && (trim($args["phone"]) == "")) {
            $e->add(t("The %s field is required.", t("Phone")));
        }
        if (in_array("phonelink", $this->btFieldsRequired) && (trim($args["phonelink"]) == "")) {
            $e->add(t("The %s field is required.", t("Phone Link")));
        }
        if (in_array("image", $this->btFieldsRequired) && (trim($args["image"]) == "" || !is_object(File::getByID($args["image"])))) {
            $e->add(t("The %s field is required.", t("Image")));
        }
        return $e;
    }

    public function composer()
    {
        $this->edit();
    }
}
