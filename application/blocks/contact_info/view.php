<?php defined("C5_EXECUTE") or die("Access Denied."); ?>

<?php if(false && isset($heading) && trim($heading) != ""): ?>
    <?=h($heading); ?>
<?php endif; ?>

<div class="col-sm-4">
	<div class="row">
		<div class="col-sm-6">
			<?php if(isset($address) && trim($address) != ""): ?>
				<span class="address"><?=h($address); ?></span>
			<?php endif; ?>
    			<?php if(isset($postal) && trim($postal) != ""): ?>
    				<span class="postal"><?=h($postal); ?></span>
    			<?php endif; ?>
		</div>
		<div class="col-sm-6">
			<img class="pull-right" src="<?=($image ? $image->getURL() : '/application/files/5914/7382/7221/celebrating-30-years.png');?>" />
		</div>
	</div>
	<div class="buttons row">
		<div class="col-sm-6">
			<?php if(isset($emaillink) && trim($emaillink) != ""): ?>
				<a class="btn btn-default" href="mailto:<?=str_replace('mailto:', '', $emaillink);?>">
					<?php if(isset($email) && trim($email) != ""): ?>
					    <?=h($email); ?>
					<?php else: ?>
						<?=$emaillink;?>
					<?php endif; ?>
				</a>
			<?php endif; ?>
		</div>
		<div class="col-sm-6">
			<?php if(isset($phonelink) && trim($phonelink) != ""): ?>
				<a class="btn btn-default" href="<?=$phonelink;?>">
					<?php if(isset($phone) && trim($phone) != ""): ?>
					    <?=h($phone); ?>
					<?php else: ?>
						<?=$phonelink;?>
					<?php endif; ?>
				</a>
			<?php endif; ?>
		</div>
	</div>
	<div class="buttons row">
		<div class="col-xs-12">
			<a class="btn btn-default" href="/about/locations">
				Servicing Australia Wide
			</a>
		</div>
	</div>
</div>
