<?php defined('C5_EXECUTE') or die("Access Denied.");
$navigationTypeText = ($navigationType == 0) ? 'arrows' : 'pages';
$c = Page::getCurrentPage();
if ($c->isEditMode()) { ?>
    <div class="ccm-edit-mode-disabled-item" style="<?php echo isset($width) ? "width: $width;" : '' ?><?php echo isset($height) ? "height: $height;" : '' ?>">
        <div style="padding: 40px 0px 40px 0px"><?php echo t('Image Slider disabled in edit mode.')?></div>
    </div>
<?php  } else { ?>

<div class="header-slider ccm-image-slider-container ccm-block-image-slider-<?=$navigationTypeText?>">
    <div class="ccm-image-slider">
        <div class="ccm-image-slider-inner">

        <?php if(count($rows) > 0) { ?>
        <ul class="rslides" id="ccm-image-slider-<?php echo $bID ?>" data-timeout="<?=$timeout;?>" data-speed="<?=$speed;?>" data-pause="<?=($pause ? 'true' : 'false');?>" data-noanimate="<?=($noAnimate ? 'true' : 'false');?>">
            <?php foreach($rows as $row) { ?>
                <li>
                <?php if($row['linkURL']) { ?>
                    <a href="<?php echo $row['linkURL'] ?>" class="mega-link-overlay">
                <?php } ?>
                <?php
                $f = File::getByID($row['fID'])
                ?>
                <?php if(is_object($f)) {
                    $tag = Core::make('html/image', array($f, false))->getTag();
                    if($row['title']) {
                        $tag->alt($row['title']);
                    }else{
                        $tag->alt("slide");
                    }
                    //print $tag;
                    ?>
                    <div class="image" style="background-image: url(<?=$tag->getAttribute('src');?>);"></div>
                <?php } ?>
                <?php if($row['linkURL']) { ?></a><?php } ?>
                <div class="ccm-image-slider-text">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12">
                                <?php if($row['title'] || (isset($row['description']) && trim(strip_tags($row['description'])) != "")): ?>
                                    <div class="header-box">
                                        <?php if($row['title']): ?>
                                            <h1 class="ccm-image-slider-title"><?php if($row['linkURL']) { ?><a href="<?php echo $row['linkURL'] ?>"><?php } ?><?php echo $row['title'] ?><?php if($row['linkURL']) { ?></a><?php } ?></h1>
                                        <?php endif; ?>
                                        <?php if(isset($row['description']) && trim(strip_tags($row['description'])) != ""): ?>
                                            <div class="description"><?=($row['description']);?></div>
                                        <?php endif; ?>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
                </li>
            <?php } ?>
        </ul>
        <?php } else { ?>
        <div class="ccm-image-slider-placeholder">
            <p><?php echo t('No Slides Entered.'); ?></p>
        </div>
        <?php } ?>
        </div>

    </div>
</div>
<?php } ?>
