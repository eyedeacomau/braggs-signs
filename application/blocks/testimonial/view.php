<?php defined('C5_EXECUTE') or die("Access Denied."); ?>
<div class="testimonial_item">
    <?php if($company): ?>
        <h2>
            <?php if($companyURL): ?><a href="<?=$companyURL;?>"><?php endif; ?>
                <?=$company;?>
            <?php if($companyURL): ?></a><?php endif; ?>
        </h2>
    <?php endif; ?>
    <?php if($paragraph): ?>
        <blockquote>
            <?php echo nl2br($paragraph); ?>
        </blockquote>
    <?php endif; ?>
    <cite>
        <?php if($name): ?><?=$name;?><?php endif; ?>
        <?php if($position): ?>, <?=$position;?><?php endif; ?>
    </cite>
    <?php if($image): ?>
        <?php if($companyURL): ?><a href="<?=$companyURL;?>"><?php endif; ?>
        <img src="<?=$image->getAttribute('src'); ?>" />
        <?php if($companyURL): ?></a><?php endif; ?>
    <?php endif; ?>
</div>