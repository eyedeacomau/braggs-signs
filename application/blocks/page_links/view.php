<?php defined("C5_EXECUTE") or die("Access Denied."); ?>

<div class="pagelinks-block">
	<?php if(!empty($pagelinks_items)): ?>
	<div class="pagelinks"<?=($background ? ' style="background-image: url('.$background->getURL().');"' : '');?>>
		<?php if(isset($listheading) && trim($listheading) != ""): ?>
		    <h2><?=h($listheading); ?></h2>
		<?php endif; ?>
		<ul>
		<?php foreach ($pagelinks_items as $pagelinks_item_key => $pagelinks_item): ?>
			<?php if(!empty($pagelinks_item["pagelink"]) && ($pagelinks_item["pagelink_c"] = Page::getByID($pagelinks_item["pagelink"])) && !$pagelinks_item["pagelink_c"]->error && !$pagelinks_item["pagelink_c"]->isInTrash()): ?>
				<li>
					<a href="<?=$pagelinks_item["pagelink_c"]->getCollectionLink();?>">
						<i class="fa fa-chevron-right"></i>
						<?=(isset($pagelinks_item["pagelink_text"]) && trim($pagelinks_item["pagelink_text"]) != "" ? $pagelinks_item["pagelink_text"] : $pagelinks_item["pagelink_c"]->getCollectionName());?>
					</a>
				</li>
			<?php endif; ?>
		<?php endforeach; ?>
		</ul>
	</div>
	<?php endif; ?>
	<?php if(isset($textheading) && trim($textheading) != ""): ?>
		<h2><?=h($textheading); ?></h2>
	<?php endif; ?>
	<?php if(isset($content) && trim($content) != ""): ?>
		<?=$content; ?>
	<?php endif; ?>
</div>