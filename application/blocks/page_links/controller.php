<?php  namespace Application\Block\PageLinks;

defined("C5_EXECUTE") or die("Access Denied.");

use Concrete\Core\Block\BlockController;
use Core;
use Concrete\Core\Editor\LinkAbstractor;
use Database;
use Page;
use File;

class Controller extends BlockController
{
    public $helpers = array('form');
    public $btFieldsRequired = array('pagelinks', 'pagelinks' => array('pagelink'));
    protected $btExportFileColumns = array('background');
    protected $btExportTables = array('btPageLinks', 'btPageLinksPagelinksEntries');
    protected $btTable = 'btPageLinks';
    protected $btInterfaceWidth = 400;
    protected $btInterfaceHeight = 500;
    protected $btIgnorePageThemeGridFrameworkContainer = false;
    protected $btCacheBlockRecord = true;
    protected $btCacheBlockOutput = true;
    protected $btCacheBlockOutputOnPost = true;
    protected $btCacheBlockOutputForRegisteredUsers = true;
    protected $btCacheBlockOutputLifetime = 0;
    protected $pkg = false;
    
    public function getBlockTypeDescription()
    {
        return t("");
    }

    public function getBlockTypeName()
    {
        return t("Page Links");
    }

    public function getSearchableContent()
    {
        $content = array();
        $content[] = $this->listheading;
        $content[] = $this->textheading;
        $content[] = $this->content;
        return implode(" ", $content);
    }

    public function view()
    {
        $db = Database::connection();
        $this->set('content', LinkAbstractor::translateFrom($this->content));
        $pagelinks = array();
        $pagelinks_items = $db->fetchAll('SELECT * FROM btPageLinksPagelinksEntries WHERE bID = ? ORDER BY sortOrder', array($this->bID));
        $this->set('pagelinks_items', $pagelinks_items);
        $this->set('pagelinks', $pagelinks);
        
        if ($this->background && ($f = File::getByID($this->background)) && is_object($f)) {
            $this->set("background", $f);
        } else {
            $this->set("background", false);
        }
    }

    public function delete()
    {
        $db = Database::connection();
        $db->delete('btPageLinksPagelinksEntries', array('bID' => $this->bID));
        parent::delete();
    }

    public function duplicate($newBID)
    {
        $db = Database::connection();
        $pagelinks_items = $db->fetchAll('SELECT * FROM btPageLinksPagelinksEntries WHERE bID = ? ORDER BY sortOrder', array($this->bID));
        foreach ($pagelinks_items as $pagelinks_item) {
            unset($pagelinks_item['id']);
            $pagelinks_item['bID'] = $newBID;
            $db->insert('btPageLinksPagelinksEntries', $pagelinks_item);
        }
        parent::duplicate($newBID);
    }

    public function add()
    {
        $this->addEdit();
        $pagelinks = $this->get('pagelinks');
        $this->set('pagelinks_items', array());
        $this->set('pagelinks', $pagelinks);
    }

    public function edit()
    {
        $db = Database::connection();
        $this->addEdit();
        
        $this->set('content', LinkAbstractor::translateFromEditMode($this->content));
        $pagelinks = $this->get('pagelinks');
        $pagelinks_items = $db->fetchAll('SELECT * FROM btPageLinksPagelinksEntries WHERE bID = ? ORDER BY sortOrder', array($this->bID));
        $this->set('pagelinks', $pagelinks);
        $this->set('pagelinks_items', $pagelinks_items);
    }

    protected function addEdit()
    {
        $pagelinks = array();
        $this->set('pagelinks', $pagelinks);
        $this->set('identifier', new \Concrete\Core\Utility\Service\Identifier());
        $al = \Concrete\Core\Asset\AssetList::getInstance();
        $al->register('css', 'repeatable-ft.form', 'blocks/page_links/css_form/repeatable-ft.form.css', array(), $this->pkg);
        $al->register('javascript', 'handlebars', 'blocks/page_links/js_form/handlebars-v4.0.4.js', array(), $this->pkg);
        $al->register('javascript', 'handlebars-helpers', 'blocks/page_links/js_form/handlebars-helpers.js', array(), $this->pkg);
        $this->requireAsset('redactor');
        $this->requireAsset('core/file-manager');
        $this->requireAsset('core/sitemap');
        $this->requireAsset('css', 'repeatable-ft.form');
        $this->requireAsset('javascript', 'handlebars');
        $this->requireAsset('javascript', 'handlebars-helpers');
        $this->set('btFieldsRequired', $this->btFieldsRequired);
    }

    public function save($args)
    {
        $db = Database::connection();
        $args['content'] = LinkAbstractor::translateTo($args['content']);
        $rows = $db->fetchAll('SELECT * FROM btPageLinksPagelinksEntries WHERE bID = ? ORDER BY sortOrder', array($this->bID));
        $pagelinks_items = isset($args['pagelinks']) && is_array($args['pagelinks']) ? $args['pagelinks'] : array();
        $queries = array();
        if (!empty($pagelinks_items)) {
            $i = 0;
            foreach ($pagelinks_items as $pagelinks_item) {
                $data = array(
                    'sortOrder' => $i + 1,
                );
                if (isset($pagelinks_item['pagelink']) && trim($pagelinks_item['pagelink']) != '' && (($pagelinks_item['pagelink_c'] = Page::getByID($pagelinks_item['pagelink'])) && !$pagelinks_item['pagelink_c']->error)) {
                    $data['pagelink'] = trim($pagelinks_item['pagelink']);
                } else {
                    $data['pagelink'] = null;
                }
                if (isset($pagelinks_item['pagelink_text']) && trim($pagelinks_item['pagelink_text']) != '') {
                    $data['pagelink_text'] = trim($pagelinks_item['pagelink_text']);
                } else {
                    $data['pagelink_text'] = null;
                }
                if (isset($rows[$i])) {
                    $queries['update'][$rows[$i]['id']] = $data;
                    unset($rows[$i]);
                } else {
                    $data['bID'] = $this->bID;
                    $queries['insert'][] = $data;
                }
                $i++;
            }
        }
        if (!empty($rows)) {
            foreach ($rows as $row) {
                $queries['delete'][] = $row['id'];
            }
        }
        if (!empty($queries)) {
            foreach ($queries as $type => $values) {
                if (!empty($values)) {
                    switch ($type) {
                        case 'update':
                            foreach ($values as $id => $data) {
                                $db->update('btPageLinksPagelinksEntries', $data, array('id' => $id));
                            }
                            break;
                        case 'insert':
                            foreach ($values as $data) {
                                $db->insert('btPageLinksPagelinksEntries', $data);
                            }
                            break;
                        case 'delete':
                            foreach ($values as $value) {
                                $db->delete('btPageLinksPagelinksEntries', array('id' => $value));
                            }
                            break;
                    }
                }
            }
        }
        parent::save($args);
    }

    public function validate($args)
    {
        $e = Core::make("helper/validation/error");
        if (in_array("listheading", $this->btFieldsRequired) && (trim($args["listheading"]) == "")) {
            $e->add(t("The %s field is required.", t("List Heading")));
        }
        if (in_array("textheading", $this->btFieldsRequired) && (trim($args["textheading"]) == "")) {
            $e->add(t("The %s field is required.", t("Text Heading")));
        }
        if (in_array("content", $this->btFieldsRequired) && (trim($args["content"]) == "")) {
            $e->add(t("The %s field is required.", t("Content")));
        }
        $pagelinksEntriesMin = 0;
        $pagelinksEntriesMax = 0;
        $pagelinksEntriesErrors = 0;
        $pagelinks = array();
        if (isset($args['pagelinks']) && is_array($args['pagelinks']) && !empty($args['pagelinks'])) {
            if ($pagelinksEntriesMin >= 1 && count($args['pagelinks']) < $pagelinksEntriesMin) {
                $e->add(t("The %s field requires at least %s entries, %s entered.", t("Page Links"), $pagelinksEntriesMin, count($args['pagelinks'])));
                $pagelinksEntriesErrors++;
            }
            if ($pagelinksEntriesMax >= 1 && count($args['pagelinks']) > $pagelinksEntriesMax) {
                $e->add(t("The %s field is set to a maximum of %s entries, %s entered.", t("Page Links"), $pagelinksEntriesMax, count($args['pagelinks'])));
                $pagelinksEntriesErrors++;
            }
            if ($pagelinksEntriesErrors == 0) {
                foreach ($args['pagelinks'] as $pagelinks_k => $pagelinks_v) {
                    if (is_array($pagelinks_v)) {
                        if ((in_array("pagelink", $this->btFieldsRequired['pagelinks']) || (isset($pagelinks_v['pagelink']) && trim($pagelinks_v['pagelink']) != '0')) && (trim($pagelinks_v['pagelink']) == "" || (($page = Page::getByID($pagelinks_v['pagelink'])) && $page->error !== false))) {
                            $e->add(t("The %s field is required (%s, row #%s).", t("Page Link"), t("Page Links"), $pagelinks_k));
                        }
                    } else {
                        $e->add(t("The values for the %s field, row #%s, are incomplete.", t('Page Links'), $pagelinks_k));
                    }
                }
            }
        } else {
            if ($pagelinksEntriesMin >= 1) {
                $e->add(t("The %s field requires at least %s entries, none entered.", t("Page Links"), $pagelinksEntriesMin));
            }
        }
        if (in_array("background", $this->btFieldsRequired) && (trim($args["background"]) == "" || !is_object(File::getByID($args["background"])))) {
            $e->add(t("The %s field is required.", t("Box Background")));
        }
        return $e;
    }

    public function composer()
    {
        $al = \Concrete\Core\Asset\AssetList::getInstance();
        $al->register('javascript', 'auto-js-page_links', 'blocks/page_links/auto.js', array(), $this->pkg);
        $this->requireAsset('javascript', 'auto-js-page_links');
        $this->edit();
    }
}