<?php  defined("C5_EXECUTE") or die("Access Denied."); ?>

<?php  $unique_tabs_id = Core::make('helper/validation/identifier')->getString(18);
$tabs = array(
    array('form-basics-' . $unique_tabs_id, t('Basics'), true),
    array('form-pagelinks_items-' . $unique_tabs_id, t('Page Links'))
);
echo Core::make('helper/concrete/ui')->tabs($tabs); ?>

<div class="ccm-tab-content" id="ccm-tab-content-form-basics-<?php  echo $unique_tabs_id; ?>">
    <div class="form-group">
    <?php  echo $form->label('listheading', t("List Heading")); ?>
    <?php  echo isset($btFieldsRequired) && in_array('listheading', $btFieldsRequired) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <?php  echo $form->text($view->field('listheading'), $listheading, array (
  'maxlength' => 255,
  'placeholder' => 'products',
)); ?>
</div><div class="form-group">
    <?php  echo $form->label('textheading', t("Text Heading")); ?>
    <?php  echo isset($btFieldsRequired) && in_array('textheading', $btFieldsRequired) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <?php  echo $form->text($view->field('textheading'), $textheading, array (
  'maxlength' => 255,
  'placeholder' => 'project overview',
)); ?>
</div><div class="form-group">
    <?php  echo $form->label('content', t("Content")); ?>
    <?php  echo isset($btFieldsRequired) && in_array('content', $btFieldsRequired) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <?php  echo Core::make('editor')->outputBlockEditModeEditor($view->field('content'), $content); ?>
</div><div class="form-group">
    <?php 
    if (isset($background) && $background > 0) {
        $background_o = File::getByID($background);
        if (!is_object($background_o)) {
            unset($background_o);
        }
    } ?>
    <?php  echo $form->label('background', t("Box Background")); ?>
    <?php  echo isset($btFieldsRequired) && in_array('background', $btFieldsRequired) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <?php  echo Core::make("helper/concrete/asset_library")->image('ccm-b-page_links-background-' . Core::make('helper/validation/identifier')->getString(18), $view->field('background'), t("Choose Image"), $background_o); ?>
</div>
</div>

<div class="ccm-tab-content" id="ccm-tab-content-form-pagelinks_items-<?php  echo $unique_tabs_id; ?>">
    <script type="text/javascript">
    var CCM_EDITOR_SECURITY_TOKEN = "<?php  echo Core::make('helper/validation/token')->generate('editor')?>";
</script>
<?php  $repeatable_container_id = 'btPageLinks-pagelinks-container-' . Core::make('helper/validation/identifier')->getString(18); ?>
    <div id="<?php  echo $repeatable_container_id; ?>">
        <div class="sortable-items-wrapper">
            <a href="#" class="btn btn-primary add-entry">
                <?php  echo t('Add Entry'); ?>
            </a>

            <div class="sortable-items" data-attr-content="<?php  echo htmlspecialchars(
                json_encode(
                    array(
                        'items' => $pagelinks_items,
                        'order' => array_keys($pagelinks_items),
                    )
                )
            ); ?>">
            </div>

            <a href="#" class="btn btn-primary add-entry add-entry-last">
                <?php  echo t('Add Entry'); ?>
            </a>
        </div>

        <script class="repeatableTemplate" type="text/x-handlebars-template">
            <div class="sortable-item" data-id="{{id}}">
                <div class="sortable-item-title">
                    <span class="sortable-item-title-default">
                        <?php  echo t('Page Links') . ' ' . t("row") . ' <span>#{{id}}</span>'; ?>
                    </span>
                    <span class="sortable-item-title-generated"></span>
                </div>

                <div class="sortable-item-inner">            <div class="form-group">
    <label for="<?php  echo $view->field('pagelinks'); ?>[{{id}}][pagelink]" class="control-label"><?php  echo t("Page Link"); ?></label>
    <?php  echo isset($btFieldsRequired['pagelinks']) && in_array('pagelink', $btFieldsRequired['pagelinks']) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <div data-page-selector="{{token}}" data-input-name="<?php  echo $view->field('pagelinks'); ?>[{{id}}][pagelink]" data-cID="{{pagelink}}" class="link-ft"></div>
</div>

<div class="form-group">
    <label for="<?php  echo $view->field('pagelinks'); ?>[{{id}}][pagelink_text]" class="control-label"><?php  echo t("Page Link") . " " . t("Text"); ?></label>
    <input name="<?php  echo $view->field('pagelinks'); ?>[{{id}}][pagelink_text]" id="<?php  echo $view->field('pagelinks'); ?>[{{id}}][pagelink_text]" class="form-control" type="text" value="{{ pagelink_text }}" />
</div></div>

                <span class="sortable-item-collapse-toggle"></span>

                <a href="#" class="sortable-item-delete" data-attr-confirm-text="<?php  echo t('Are you sure'); ?>">
                    <i class="fa fa-times"></i>
                </a>

                <div class="sortable-item-handle">
                    <i class="fa fa-sort"></i>
                </div>
            </div>
        </script>
    </div>

<script type="text/javascript">
    Concrete.event.publish('btPageLinks.pagelinks.edit.open', {id: '<?php  echo $repeatable_container_id; ?>'});
    $.each($('#<?php  echo $repeatable_container_id; ?> input[type="text"].title-me'), function () {
        $(this).trigger('keyup');
    });
</script>
</div>