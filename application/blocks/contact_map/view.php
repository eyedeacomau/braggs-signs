<?php defined("C5_EXECUTE") or die("Access Denied.");
$c = Page::getCurrentPage();
$u = new User();
if(!function_exists('slug')) {
    function slug($str, $separator = '-') {
        $clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $str);
        $clean = strtolower(trim($clean, $separator));
        return preg_replace("/[\/_|+ -]+/", $separator, $clean);
    }
}
if(!empty($markers_items)) {
    $markers = array();
    $i = 1;
    foreach($markers_items as $location) {
        if(isset($location["lat"]) && trim($location["lat"]) != "" && isset($location["lng"]) && trim($location["lng"]) != "") {
            $location['id'] = $i;
            $markers[] = array(
                //'id' => $location['id'],
                'lat' => $location['lat'],
                'lng' => $location['lng'],
                'icon' => $view->getThemePath().'/images/contact-marker.png',
                'title' => $location['title'],
                'infoWindow' => array(
                    'content' => '<h2>'.h($location["title"]).'</h2><p>'.nl2br(h($location["popup"])).'</p>'
                )
            );
        }
        $i++;
    }
}
?>
<?php if((is_object($c) && $c->isEditMode()) || $u->isLoggedIn()): ?>
    <div class="ccm-edit-mode-disabled-item"><?php echo t('Contact Map Edit Mode')?></div>
<?php else: ?>
<script type="text/javascript">
    var markers = <?=json_encode($markers);?>;
</script>
<script type="text/javascript" src="//maps.googleapis.com/maps/api/js?key=AIzaSyDn-cpW6RPUT316NEfBMb-MN0cF6JV9Lvo"></script>
<script type="text/javascript" src="<?=$view->getThemePath(); ?>/js/gmaps.js"></script>
<div id="map-block" class="container-fluid">
    <div class="row">
        <div id="map-holder" class="col-sm-6">
            <div id="map"></div>
        </div>
        <div class="col-sm-6">
            <div class="row">
                <div class="col-md-10 col-lg-8 contact-info">
                    <?php if(isset($heading) && trim($heading) != ""): ?>
                        <h1><?=h($heading); ?></h1>
                    <?php endif; ?>
                    <dl>
                        <?php if(isset($phone) && trim($phone) != ""): ?>
                            <dt>Phone:</dt>
                            <dd><?=h($phone); ?></dd>
                        <?php endif; ?>
                        <?php if(isset($fax) && trim($fax) != ""): ?>
                            <dt>Fax:</dt>
                            <dd><?=h($fax); ?></dd>
                        <?php endif; ?>
                        <?php if(isset($Email) && trim($Email) != ""): ?>
                            <dt>Email:</dt>
                            <dd><a href="mailto:<?=h($Email); ?>"><?=h($Email); ?></a></dd>
                        <?php endif; ?>
                        <?php if(isset($address) && trim($address) != ""): ?>
                            <dt>Address:</dt>
                            <dd><?=nl2br(h($address)); ?></dd>
                        <?php endif; ?>
                    </dl>
                    <?php if(isset($content) && trim($content) != ""): ?>
                        <?=$content; ?>
                    <?php endif; ?>
                    <br /><br />
                </div>
            </div>
        </div>
    </div>
</div>
<?php endif; ?>