<?php  namespace Application\Block\ContactMap;

defined("C5_EXECUTE") or die("Access Denied.");

use Concrete\Core\Block\BlockController;
use Core;
use Database;
use Concrete\Core\Editor\LinkAbstractor;

class Controller extends BlockController
{
    public $helpers = array('form');
    public $btFieldsRequired = array('markers', 'markers' => array('lat'));
    protected $btExportFileColumns = array();
    protected $btExportTables = array('btContactMap', 'btContactMapMarkersEntries');
    protected $btTable = 'btContactMap';
    protected $btInterfaceWidth = 400;
    protected $btInterfaceHeight = 500;
    protected $btIgnorePageThemeGridFrameworkContainer = true;
    protected $btCacheBlockRecord = true;
    protected $btCacheBlockOutput = true;
    protected $btCacheBlockOutputOnPost = true;
    protected $btCacheBlockOutputForRegisteredUsers = true;
    protected $btCacheBlockOutputLifetime = 0;
    protected $pkg = false;
    
    public function getBlockTypeDescription()
    {
        return t("");
    }

    public function getBlockTypeName()
    {
        return t("Contact Map");
    }

    public function getSearchableContent()
    {
        $content = array();
        $db = Database::connection();
        $markers_items = $db->fetchAll('SELECT * FROM btContactMapMarkersEntries WHERE bID = ? ORDER BY sortOrder', array($this->bID));
        foreach ($markers_items as $markers_item_k => $markers_item_v) {
            if (isset($markers_item_v["title"]) && trim($markers_item_v["title"]) != "") {
                $content[] = $markers_item_v["title"];
            }
            if (isset($markers_item_v["lat"]) && trim($markers_item_v["lat"]) != "") {
                $content[] = $markers_item_v["lat"];
            }
            if (isset($markers_item_v["lng"]) && trim($markers_item_v["lng"]) != "") {
                $content[] = $markers_item_v["lng"];
            }
            if (isset($markers_item_v["popup"]) && trim($markers_item_v["popup"]) != "") {
                $content[] = $markers_item_v["popup"];
            }
        }
        $content[] = $this->heading;
        $content[] = $this->phone;
        $content[] = $this->fax;
        $content[] = $this->Email;
        $content[] = $this->address;
        $content[] = $this->content;
        return implode(" ", $content);
    }

    public function view()
    {
        $db = Database::connection();
        $markers = array();
        $markers_items = $db->fetchAll('SELECT * FROM btContactMapMarkersEntries WHERE bID = ? ORDER BY sortOrder', array($this->bID));
        $this->set('markers_items', $markers_items);
        $this->set('markers', $markers);
        if (trim($this->heading) == "") {
            $this->set("heading", 'contact us');
        }
        $this->set('content', LinkAbstractor::translateFrom($this->content));
    }

    public function delete()
    {
        $db = Database::connection();
        $db->delete('btContactMapMarkersEntries', array('bID' => $this->bID));
        parent::delete();
    }

    public function duplicate($newBID)
    {
        $db = Database::connection();
        $markers_items = $db->fetchAll('SELECT * FROM btContactMapMarkersEntries WHERE bID = ? ORDER BY sortOrder', array($this->bID));
        foreach ($markers_items as $markers_item) {
            unset($markers_item['id']);
            $markers_item['bID'] = $newBID;
            $db->insert('btContactMapMarkersEntries', $markers_item);
        }
        parent::duplicate($newBID);
    }

    public function add()
    {
        $this->addEdit();
        $markers = $this->get('markers');
        $this->set('markers_items', array());
        $this->set('markers', $markers);
    }

    public function edit()
    {
        $db = Database::connection();
        $this->addEdit();
        $markers = $this->get('markers');
        $markers_items = $db->fetchAll('SELECT * FROM btContactMapMarkersEntries WHERE bID = ? ORDER BY sortOrder', array($this->bID));
        $this->set('markers', $markers);
        $this->set('markers_items', $markers_items);
        
        $this->set('content', LinkAbstractor::translateFromEditMode($this->content));
    }

    protected function addEdit()
    {
        $markers = array();
        $this->set('markers', $markers);
        $this->set('identifier', new \Concrete\Core\Utility\Service\Identifier());
        $al = \Concrete\Core\Asset\AssetList::getInstance();
        $al->register('css', 'repeatable-ft.form', 'blocks/contact_map/css_form/repeatable-ft.form.css', array(), $this->pkg);
        $al->register('javascript', 'handlebars', 'blocks/contact_map/js_form/handlebars-v4.0.4.js', array(), $this->pkg);
        $al->register('javascript', 'handlebars-helpers', 'blocks/contact_map/js_form/handlebars-helpers.js', array(), $this->pkg);
        $this->requireAsset('core/sitemap');
        $this->requireAsset('css', 'repeatable-ft.form');
        $this->requireAsset('javascript', 'handlebars');
        $this->requireAsset('javascript', 'handlebars-helpers');
        $this->requireAsset('redactor');
        $this->requireAsset('core/file-manager');
        $this->set('btFieldsRequired', $this->btFieldsRequired);
    }

    public function save($args)
    {
        $db = Database::connection();
        $rows = $db->fetchAll('SELECT * FROM btContactMapMarkersEntries WHERE bID = ? ORDER BY sortOrder', array($this->bID));
        $markers_items = isset($args['markers']) && is_array($args['markers']) ? $args['markers'] : array();
        $queries = array();
        if (!empty($markers_items)) {
            $i = 0;
            foreach ($markers_items as $markers_item) {
                $data = array(
                    'sortOrder' => $i + 1,
                );
                if (isset($markers_item['title']) && trim($markers_item['title']) != '') {
                    $data['title'] = trim($markers_item['title']);
                } else {
                    $data['title'] = null;
                }
                if (isset($markers_item['lat']) && trim($markers_item['lat']) != '') {
                    $data['lat'] = trim($markers_item['lat']);
                } else {
                    $data['lat'] = null;
                }
                if (isset($markers_item['lng']) && trim($markers_item['lng']) != '') {
                    $data['lng'] = trim($markers_item['lng']);
                } else {
                    $data['lng'] = null;
                }
                if (isset($markers_item['popup']) && trim($markers_item['popup']) != '') {
                    $data['popup'] = trim($markers_item['popup']);
                } else {
                    $data['popup'] = null;
                }
                if (isset($rows[$i])) {
                    $queries['update'][$rows[$i]['id']] = $data;
                    unset($rows[$i]);
                } else {
                    $data['bID'] = $this->bID;
                    $queries['insert'][] = $data;
                }
                $i++;
            }
        }
        if (!empty($rows)) {
            foreach ($rows as $row) {
                $queries['delete'][] = $row['id'];
            }
        }
        if (!empty($queries)) {
            foreach ($queries as $type => $values) {
                if (!empty($values)) {
                    switch ($type) {
                        case 'update':
                            foreach ($values as $id => $data) {
                                $db->update('btContactMapMarkersEntries', $data, array('id' => $id));
                            }
                            break;
                        case 'insert':
                            foreach ($values as $data) {
                                $db->insert('btContactMapMarkersEntries', $data);
                            }
                            break;
                        case 'delete':
                            foreach ($values as $value) {
                                $db->delete('btContactMapMarkersEntries', array('id' => $value));
                            }
                            break;
                    }
                }
            }
        }
        $args['content'] = LinkAbstractor::translateTo($args['content']);
        parent::save($args);
    }

    public function validate($args)
    {
        $e = Core::make("helper/validation/error");
        $markersEntriesMin = 0;
        $markersEntriesMax = 0;
        $markersEntriesErrors = 0;
        $markers = array();
        if (isset($args['markers']) && is_array($args['markers']) && !empty($args['markers'])) {
            if ($markersEntriesMin >= 1 && count($args['markers']) < $markersEntriesMin) {
                $e->add(t("The %s field requires at least %s entries, %s entered.", t("Markers"), $markersEntriesMin, count($args['markers'])));
                $markersEntriesErrors++;
            }
            if ($markersEntriesMax >= 1 && count($args['markers']) > $markersEntriesMax) {
                $e->add(t("The %s field is set to a maximum of %s entries, %s entered.", t("Markers"), $markersEntriesMax, count($args['markers'])));
                $markersEntriesErrors++;
            }
            if ($markersEntriesErrors == 0) {
                foreach ($args['markers'] as $markers_k => $markers_v) {
                    if (is_array($markers_v)) {
                        if (in_array("title", $this->btFieldsRequired['markers']) && (!isset($markers_v['title']) || trim($markers_v['title']) == "")) {
                            $e->add(t("The %s field is required (%s, row #%s).", t("Title"), t("Markers"), $markers_k));
                        }
                        if (in_array("lat", $this->btFieldsRequired['markers']) && (!isset($markers_v['lat']) || trim($markers_v['lat']) == "")) {
                            $e->add(t("The %s field is required (%s, row #%s).", t("Latitude"), t("Markers"), $markers_k));
                        }
                        if (in_array("lng", $this->btFieldsRequired['markers']) && (!isset($markers_v['lng']) || trim($markers_v['lng']) == "")) {
                            $e->add(t("The %s field is required (%s, row #%s).", t("Longitude"), t("Markers"), $markers_k));
                        }
                        if (in_array("popup", $this->btFieldsRequired['markers']) && (!isset($markers_v['popup']) || trim($markers_v['popup']) == "")) {
                            $e->add(t("The %s field is required (%s, row #%s).", t("Marker Popup Window"), t("Markers"), $markers_k));
                        }
                    } else {
                        $e->add(t("The values for the %s field, row #%s, are incomplete.", t('Markers'), $markers_k));
                    }
                }
            }
        } else {
            if ($markersEntriesMin >= 1) {
                $e->add(t("The %s field requires at least %s entries, none entered.", t("Markers"), $markersEntriesMin));
            }
        }
        if (in_array("heading", $this->btFieldsRequired) && (trim($args["heading"]) == "")) {
            $e->add(t("The %s field is required.", t("Heading")));
        }
        if (in_array("phone", $this->btFieldsRequired) && (trim($args["phone"]) == "")) {
            $e->add(t("The %s field is required.", t("Phone")));
        }
        if (in_array("fax", $this->btFieldsRequired) && (trim($args["fax"]) == "")) {
            $e->add(t("The %s field is required.", t("Fax")));
        }
        if (in_array("Email", $this->btFieldsRequired) && (trim($args["Email"]) == "")) {
            $e->add(t("The %s field is required.", t("Email")));
        }
        if (in_array("address", $this->btFieldsRequired) && trim($args["address"]) == "") {
            $e->add(t("The %s field is required.", t("Address")));
        }
        if (in_array("content", $this->btFieldsRequired) && (trim($args["content"]) == "")) {
            $e->add(t("The %s field is required.", t("Content")));
        }
        return $e;
    }

    public function composer()
    {
        $al = \Concrete\Core\Asset\AssetList::getInstance();
        $al->register('javascript', 'auto-js-contact_map', 'blocks/contact_map/auto.js', array(), $this->pkg);
        $this->requireAsset('javascript', 'auto-js-contact_map');
        $this->edit();
    }
}