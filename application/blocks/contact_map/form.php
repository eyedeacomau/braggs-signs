<?php  defined("C5_EXECUTE") or die("Access Denied."); ?>

<?php  $unique_tabs_id = Core::make('helper/validation/identifier')->getString(18);
$tabs = array(
    array('form-basics-' . $unique_tabs_id, t('Basics'), true),
    array('form-markers_items-' . $unique_tabs_id, t('Markers'))
);
echo Core::make('helper/concrete/ui')->tabs($tabs); ?>

<div class="ccm-tab-content" id="ccm-tab-content-form-basics-<?php  echo $unique_tabs_id; ?>">
    <div class="form-group">
    <?php  echo $form->label('heading', t("Heading")); ?>
    <?php  echo isset($btFieldsRequired) && in_array('heading', $btFieldsRequired) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <?php  echo $form->text($view->field('heading'), $heading, array (
  'maxlength' => 255,
  'placeholder' => 'contact us',
)); ?>
</div><div class="form-group">
    <?php  echo $form->label('phone', t("Phone")); ?>
    <?php  echo isset($btFieldsRequired) && in_array('phone', $btFieldsRequired) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <?php  echo $form->text($view->field('phone'), $phone, array (
  'maxlength' => 255,
)); ?>
</div><div class="form-group" style="display: none;">
    <?php  echo $form->label('fax', t("Fax")); ?>
    <?php  echo isset($btFieldsRequired) && in_array('fax', $btFieldsRequired) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <?php  echo $form->text($view->field('fax'), $fax, array (
  'maxlength' => 255,
)); ?>
</div><div class="form-group">
    <?php  echo $form->label('Email', t("Email")); ?>
    <?php  echo isset($btFieldsRequired) && in_array('Email', $btFieldsRequired) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <?php  echo $form->text($view->field('Email'), $Email, array (
  'maxlength' => 255,
)); ?>
</div><div class="form-group">
    <?php  echo $form->label('address', t("Address")); ?>
    <?php  echo isset($btFieldsRequired) && in_array('address', $btFieldsRequired) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <?php  echo $form->textarea($view->field('address'), $address, array (
  'rows' => 5,
)); ?>
</div><div class="form-group">
    <?php  echo $form->label('content', t("Content")); ?>
    <?php  echo isset($btFieldsRequired) && in_array('content', $btFieldsRequired) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <?php  echo Core::make('editor')->outputBlockEditModeEditor($view->field('content'), $content); ?>
</div>
</div>

<div class="ccm-tab-content" id="ccm-tab-content-form-markers_items-<?php  echo $unique_tabs_id; ?>">
    <script type="text/javascript">
    var CCM_EDITOR_SECURITY_TOKEN = "<?php  echo Core::make('helper/validation/token')->generate('editor')?>";
</script>
<?php  $repeatable_container_id = 'btContactMap-markers-container-' . Core::make('helper/validation/identifier')->getString(18); ?>
    <div id="<?php  echo $repeatable_container_id; ?>">
        <div class="sortable-items-wrapper">
            <a href="#" class="btn btn-primary add-entry">
                <?php  echo t('New Marker'); ?>
            </a>

            <div class="sortable-items" data-attr-content="<?php  echo htmlspecialchars(
                json_encode(
                    array(
                        'items' => $markers_items,
                        'order' => array_keys($markers_items),
                    )
                )
            ); ?>">
            </div>

            <a href="#" class="btn btn-primary add-entry add-entry-last">
                <?php  echo t('New Marker'); ?>
            </a>
        </div>

        <script class="repeatableTemplate" type="text/x-handlebars-template">
            <div class="sortable-item" data-id="{{id}}">
                <div class="sortable-item-title">
                    <span class="sortable-item-title-default">
                        <?php  echo t('Markers') . ' ' . t("row") . ' <span>#{{id}}</span>'; ?>
                    </span>
                    <span class="sortable-item-title-generated"></span>
                </div>

                <div class="sortable-item-inner">            <div class="form-group">
    <label for="<?php  echo $view->field('markers'); ?>[{{id}}][title]" class="control-label"><?php  echo t("Title"); ?></label>
    <?php  echo isset($btFieldsRequired['markers']) && in_array('title', $btFieldsRequired['markers']) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <input name="<?php  echo $view->field('markers'); ?>[{{id}}][title]" id="<?php  echo $view->field('markers'); ?>[{{id}}][title]" class="form-control title-me" type="text" value="{{ title }}" maxlength="255" />
</div>            <div class="form-group">
    <label for="<?php  echo $view->field('markers'); ?>[{{id}}][lat]" class="control-label"><?php  echo t("Latitude"); ?></label>
    <?php  echo isset($btFieldsRequired['markers']) && in_array('lat', $btFieldsRequired['markers']) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <input name="<?php  echo $view->field('markers'); ?>[{{id}}][lat]" id="<?php  echo $view->field('markers'); ?>[{{id}}][lat]" class="form-control" type="text" value="{{ lat }}" maxlength="255" placeholder="-27.000000" />
</div>            <div class="form-group">
    <label for="<?php  echo $view->field('markers'); ?>[{{id}}][lng]" class="control-label"><?php  echo t("Longitude"); ?></label>
    <?php  echo isset($btFieldsRequired['markers']) && in_array('lng', $btFieldsRequired['markers']) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <input name="<?php  echo $view->field('markers'); ?>[{{id}}][lng]" id="<?php  echo $view->field('markers'); ?>[{{id}}][lng]" class="form-control" type="text" value="{{ lng }}" maxlength="255" placeholder="153.000000" />
</div>            <div class="form-group">
    <label for="<?php  echo $view->field('markers'); ?>[{{id}}][popup]" class="control-label"><?php  echo t("Marker Popup Window"); ?></label>
    <?php  echo isset($btFieldsRequired['markers']) && in_array('popup', $btFieldsRequired['markers']) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <textarea name="<?php  echo $view->field('markers'); ?>[{{id}}][popup]" id="<?php  echo $view->field('markers'); ?>[{{id}}][popup]" class="form-control" rows="5">{{ popup }}</textarea>
</div></div>

                <span class="sortable-item-collapse-toggle"></span>

                <a href="#" class="sortable-item-delete" data-attr-confirm-text="<?php  echo t('Are you sure'); ?>">
                    <i class="fa fa-times"></i>
                </a>

                <div class="sortable-item-handle">
                    <i class="fa fa-sort"></i>
                </div>
            </div>
        </script>
    </div>

<script type="text/javascript">
    Concrete.event.publish('btContactMap.markers.edit.open', {id: '<?php  echo $repeatable_container_id; ?>'});
    $.each($('#<?php  echo $repeatable_container_id; ?> input[type="text"].title-me'), function () {
        $(this).trigger('keyup');
    });
</script>
</div>