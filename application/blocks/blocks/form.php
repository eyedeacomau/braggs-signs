<?php  defined("C5_EXECUTE") or die("Access Denied."); ?>

<?php  $unique_tabs_id = Core::make('helper/validation/identifier')->getString(18);
$tabs = array(
    array('form-basics-' . $unique_tabs_id, t('Basics'), true),
    array('form-blocks_items-' . $unique_tabs_id, t('Blocks'))
);
echo Core::make('helper/concrete/ui')->tabs($tabs); ?>

<div class="ccm-tab-content" id="ccm-tab-content-form-basics-<?php  echo $unique_tabs_id; ?>">
    <div class="form-group">
    <?php  echo $form->label('layout', t("Layout")); ?>
    <?php  echo isset($btFieldsRequired) && in_array('layout', $btFieldsRequired) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <?php  echo $form->select($view->field('layout'), $layout_options, $layout, array()); ?>
</div>
</div>

<div class="ccm-tab-content" id="ccm-tab-content-form-blocks_items-<?php  echo $unique_tabs_id; ?>">
    <script type="text/javascript">
    var CCM_EDITOR_SECURITY_TOKEN = "<?php  echo Core::make('helper/validation/token')->generate('editor')?>";
</script>
<?php  $repeatable_container_id = 'btBlocks-blocks-container-' . Core::make('helper/validation/identifier')->getString(18); ?>
    <div id="<?php  echo $repeatable_container_id; ?>">
        <div class="sortable-items-wrapper">
            <a href="#" class="btn btn-primary add-entry">
                <?php  echo t('Add Entry'); ?>
            </a>

            <div class="sortable-items" data-attr-content="<?php  echo htmlspecialchars(
                json_encode(
                    array(
                        'items' => $blocks_items,
                        'order' => array_keys($blocks_items),
                    )
                )
            ); ?>">
            </div>

            <a href="#" class="btn btn-primary add-entry add-entry-last">
                <?php  echo t('Add Entry'); ?>
            </a>
        </div>

        <script class="repeatableTemplate" type="text/x-handlebars-template">
            <div class="sortable-item" data-id="{{id}}">
                <div class="sortable-item-title">
                    <span class="sortable-item-title-default">
                        <?php  echo t('Blocks') . ' ' . t("row") . ' <span>#{{id}}</span>'; ?>
                    </span>
                    <span class="sortable-item-title-generated"></span>
                </div>

                <div class="sortable-item-inner">            <div class="form-group">
    <label for="<?php  echo $view->field('blocks'); ?>[{{id}}][title]" class="control-label"><?php  echo t("Title"); ?></label>
    <?php  echo isset($btFieldsRequired['blocks']) && in_array('title', $btFieldsRequired['blocks']) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <input name="<?php  echo $view->field('blocks'); ?>[{{id}}][title]" id="<?php  echo $view->field('blocks'); ?>[{{id}}][title]" class="form-control title-me" type="text" value="{{ title }}" maxlength="255" />
</div>            <div class="form-group">
    <label for="<?php  echo $view->field('blocks'); ?>[{{id}}][details]" class="control-label"><?php  echo t("Details"); ?></label>
    <?php  echo isset($btFieldsRequired['blocks']) && in_array('details', $btFieldsRequired['blocks']) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <textarea name="<?php  echo $view->field('blocks'); ?>[{{id}}][details]" id="<?php  echo $view->field('blocks'); ?>[{{id}}][details]" class="form-control" rows="5">{{ details }}</textarea>
</div>            <div class="form-group">
    <label for="<?php  echo $view->field('blocks'); ?>[{{id}}][pagelink]" class="control-label"><?php  echo t("Page Link"); ?></label>
    <?php  echo isset($btFieldsRequired['blocks']) && in_array('pagelink', $btFieldsRequired['blocks']) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <div data-page-selector="{{token}}" data-input-name="<?php  echo $view->field('blocks'); ?>[{{id}}][pagelink]" data-cID="{{pagelink}}" class="link-ft"></div>
</div>            <div class="form-group">
    <label for="<?php  echo $view->field('blocks'); ?>[{{id}}][external]" class="control-label"><?php  echo t("External Link"); ?></label>
    <?php  echo isset($btFieldsRequired['blocks']) && in_array('external', $btFieldsRequired['blocks']) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <input name="<?php  echo $view->field('blocks'); ?>[{{id}}][external]" id="<?php  echo $view->field('blocks'); ?>[{{id}}][external]" class="form-control" type="text" value="{{ external }}" />
</div>            <div class="form-group">
    <label for="<?php  echo $view->field('blocks'); ?>[{{id}}][readmore]" class="control-label"><?php  echo t("Read More Text"); ?></label>
    <?php  echo isset($btFieldsRequired['blocks']) && in_array('readmore', $btFieldsRequired['blocks']) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <input name="<?php  echo $view->field('blocks'); ?>[{{id}}][readmore]" id="<?php  echo $view->field('blocks'); ?>[{{id}}][readmore]" class="form-control" type="text" value="{{ readmore }}" maxlength="255" placeholder="See Projects" />
</div>            <div class="form-group">
    <label for="<?php  echo $view->field('blocks'); ?>[{{id}}][background]" class="control-label"><?php  echo t("Background"); ?></label>
    <?php  echo isset($btFieldsRequired['blocks']) && in_array('background', $btFieldsRequired['blocks']) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <div data-file-selector-input-name="<?php  echo $view->field('blocks'); ?>[{{id}}][background]" class="ccm-file-selector ft-image-background-file-selector" data-file-selector-f-id="{{ background }}"></div>
</div>            

<div style="display: none;">
                <div class="form-group">
                    <label for="<?php  echo $view->field('blocks'); ?>[{{id}}][extratext]" class="control-label"><?php  echo t("Extra Text"); ?></label>
                    <?php  echo isset($btFieldsRequired['blocks']) && in_array('extratext', $btFieldsRequired['blocks']) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
                    <input name="<?php  echo $view->field('blocks'); ?>[{{id}}][extratext]" id="<?php  echo $view->field('blocks'); ?>[{{id}}][extratext]" class="form-control" type="text" value="{{ extratext }}" maxlength="255" />
                </div>            <div class="form-group">
                    <label for="<?php  echo $view->field('blocks'); ?>[{{id}}][extratextarea]" class="control-label"><?php  echo t("Extra Textarea"); ?></label>
                    <?php  echo isset($btFieldsRequired['blocks']) && in_array('extratextarea', $btFieldsRequired['blocks']) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
                    <textarea name="<?php  echo $view->field('blocks'); ?>[{{id}}][extratextarea]" id="<?php  echo $view->field('blocks'); ?>[{{id}}][extratextarea]" class="form-control" rows="5">{{ extratextarea }}</textarea>
                </div>            <div class="form-group">
                    <label for="<?php  echo $view->field('blocks'); ?>[{{id}}][extraurl]" class="control-label"><?php  echo t("Extra URL"); ?></label>
                    <?php  echo isset($btFieldsRequired['blocks']) && in_array('extraurl', $btFieldsRequired['blocks']) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
                    <input name="<?php  echo $view->field('blocks'); ?>[{{id}}][extraurl]" id="<?php  echo $view->field('blocks'); ?>[{{id}}][extraurl]" class="form-control" type="text" value="{{ extraurl }}" />
                </div>            <div class="form-group">
                    <label for="<?php  echo $view->field('blocks'); ?>[{{id}}][extraselect]" class="control-label"><?php  echo t("Extra Select"); ?></label>
                    <?php  echo isset($btFieldsRequired['blocks']) && in_array('extraselect', $btFieldsRequired['blocks']) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
                    <?php  $blocksExtraselect_options = $blocks['extraselect_options']; ?>
                                    <select name="<?php  echo $view->field('blocks'); ?>[{{id}}][extraselect]" id="<?php  echo $view->field('blocks'); ?>[{{id}}][extraselect]" class="form-control">{{#select extraselect}}<?php  foreach ($blocksExtraselect_options as $k => $v) {
                                        echo "<option value='" . $k . "'>" . $v . "</option>";
                                     } ?>{{/select}}</select>
                </div>            <div class="form-group">
                    <label for="<?php  echo $view->field('blocks'); ?>[{{id}}][extraboolean]" class="control-label"><?php  echo t("Extra Boolean"); ?></label>
                    <?php  echo isset($btFieldsRequired) && in_array('extraboolean', $btFieldsRequired) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
                    <?php  $blocksExtraboolean_options = (isset($btFieldsRequired['blocks']) && in_array('extraboolean', $btFieldsRequired['blocks']) ? array() : array("" => "--" . t("Select") . "--")) + array(0 => t("No"), 1 => t("Yes")); ?>
                                    <select name="<?php  echo $view->field('blocks'); ?>[{{id}}][extraboolean]" id="<?php  echo $view->field('blocks'); ?>[{{id}}][extraboolean]" class="form-control">{{#select extraboolean}}<?php  foreach ($blocksExtraboolean_options as $k => $v) {
                                        echo "<option value='" . $k . "'>" . $v . "</option>";
                                     } ?>{{/select}}</select>
                </div>            <div class="form-group">
                    <label for="<?php  echo $view->field('blocks'); ?>[{{id}}][extraimage]" class="control-label"><?php  echo t("Extra Image"); ?></label>
                    <?php  echo isset($btFieldsRequired['blocks']) && in_array('extraimage', $btFieldsRequired['blocks']) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
                    <div data-file-selector-input-name="<?php  echo $view->field('blocks'); ?>[{{id}}][extraimage]" class="ccm-file-selector ft-image-extraimage-file-selector" data-file-selector-f-id="{{ extraimage }}"></div>
                </div>
</div>

</div>

                <span class="sortable-item-collapse-toggle"></span>

                <a href="#" class="sortable-item-delete" data-attr-confirm-text="<?php  echo t('Are you sure'); ?>">
                    <i class="fa fa-times"></i>
                </a>

                <div class="sortable-item-handle">
                    <i class="fa fa-sort"></i>
                </div>
            </div>
        </script>
    </div>

<script type="text/javascript">
    Concrete.event.publish('btBlocks.blocks.edit.open', {id: '<?php  echo $repeatable_container_id; ?>'});
    $.each($('#<?php  echo $repeatable_container_id; ?> input[type="text"].title-me'), function () {
        $(this).trigger('keyup');
    });
</script>
</div>