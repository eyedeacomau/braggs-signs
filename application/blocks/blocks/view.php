<?php defined("C5_EXECUTE") or die("Access Denied."); ?>

<?php if(!empty($blocks_items)): ?>
    <div class="blocks container-fluid <?=(isset($layout) && trim($layout) != "" && array_key_exists($layout, $layout_options) ? $layout : '');?>">
        <div class="row">
            <?php $i = 0; foreach($blocks_items as $blocks_item_key => $blocks_item): ?>
                <?php
                    $link = false;
                    $target = false;
                    if(!empty($blocks_item["pagelink"]) && ($blocks_item["pagelink_c"] = Page::getByID($blocks_item["pagelink"])) && !$blocks_item["pagelink_c"]->error && !$blocks_item["pagelink_c"]->isInTrash()) {
                        $link = $blocks_item["pagelink_c"]->getCollectionLink();
                    }
                    if(isset($blocks_item["external"]) && trim($blocks_item["external"]) != "") {
                        $link = $blocks_item["external"];
                        $target = '_blank';
                    }
                ?>
                <div class="block col-xs-12 <?=($layout == 'orange' ? 'col-sm-6' : 'col-sm-12');?> col-md-4"<?php if($blocks_item["background"]): ?> style="background-image: url(<?=$blocks_item["background"]->getURL(); ?>);"<?php endif; ?>>
                    <?php if($link != false): ?>
                        <a class="item" href="<?=$link;?>"<?=($target != false ? ' target="'.$target.'"' : '');?>>
                    <?php else: ?>
                        <div class="item">
                    <?php endif; ?>
                        <?php if(isset($blocks_item["title"]) && trim($blocks_item["title"]) != ""): ?>
                            <span class="title"><?=h($blocks_item["title"]); ?></span>
                        <?php endif; ?>
                        <?php if(isset($blocks_item["details"]) && trim($blocks_item["details"]) != ""): ?>
                            <div class="details"><?=nl2br(h($blocks_item["details"])); ?></div>
                        <?php endif; ?>
                        <?php if(isset($blocks_item["readmore"]) && trim($blocks_item["readmore"]) != ""): ?>
                            <span class="readmore"><?=h($blocks_item["readmore"]); ?> <i class="fa fa-long-arrow-right"></i></span>
                        <?php endif; ?>
                    <?php if($link != false): ?>
                        </a>
                    <?php else: ?>
                        </div>
                    <?php endif; ?>
                </div>

<?php if(false): // CN 2016-09-19 extras ?>
<?php if(isset($blocks_item["extratext"]) && trim($blocks_item["extratext"]) != ""): ?>
<?=h($blocks_item["extratext"]); ?><?php endif; ?><?php if(isset($blocks_item["extratextarea"]) && trim($blocks_item["extratextarea"]) != ""): ?>
<?=h($blocks_item["extratextarea"]); ?><?php endif; ?><?php if(isset($blocks_item["extraurl"]) && trim($blocks_item["extraurl"]) != ""): ?>
<?="<a href=\"" . $blocks_item["extraurl"] . "\"></a>"; ?><?php endif; ?><?php if(trim($blocks_item["extraselect"]) != ""): ?>

<?php switch($blocks_item["extraselect"]) {
case "foo":
// ENTER MARKUP HERE FOR FIELD "Extra Select" : CHOICE "Foo"
break;
case "bar":
// ENTER MARKUP HERE FOR FIELD "Extra Select" : CHOICE "Bar"
break;
                            } ?><?php endif; ?><?php if(isset($blocks_item["extraboolean"]) && trim($blocks_item["extraboolean"]) != ""): ?>
<?=$blocks_item["extraboolean"] == 1 ? t("Yes") : t("No"); ?><?php endif; ?><?php if($blocks_item["extraimage"]): ?>
<?=$blocks_item["extraimage"]->getURL(); ?><?php endif; ?>
<?php endif; ?>
            <?php endforeach; ?>
        </div>
    </div>
<?php endif; ?>


