<?php  namespace Application\Block\Blocks;

defined("C5_EXECUTE") or die("Access Denied.");

use Concrete\Core\Block\BlockController;
use Core;
use Database;
use Page;
use File;

class Controller extends BlockController
{
    public $helpers = array('form');
    public $btFieldsRequired = array('layout', 'blocks', 'blocks' => array('title'));
    protected $btExportFileColumns = array();
    protected $btExportTables = array('btBlocks', 'btBlocksBlocksEntries');
    protected $btTable = 'btBlocks';
    protected $btInterfaceWidth = 400;
    protected $btInterfaceHeight = 500;
    protected $btIgnorePageThemeGridFrameworkContainer = true;
    protected $btCacheBlockRecord = true;
    protected $btCacheBlockOutput = true;
    protected $btCacheBlockOutputOnPost = true;
    protected $btCacheBlockOutputForRegisteredUsers = true;
    protected $btCacheBlockOutputLifetime = 0;
    protected $pkg = false;
    
    public function getBlockTypeDescription()
    {
        return t("");
    }

    public function getBlockTypeName()
    {
        return t("Blocks");
    }

    public function getSearchableContent()
    {
        $content = array();
        $db = Database::connection();
        $blocks_items = $db->fetchAll('SELECT * FROM btBlocksBlocksEntries WHERE bID = ? ORDER BY sortOrder', array($this->bID));
        foreach ($blocks_items as $blocks_item_k => $blocks_item_v) {
            if (isset($blocks_item_v["title"]) && trim($blocks_item_v["title"]) != "") {
                $content[] = $blocks_item_v["title"];
            }
            if (isset($blocks_item_v["details"]) && trim($blocks_item_v["details"]) != "") {
                $content[] = $blocks_item_v["details"];
            }
            if (isset($blocks_item_v["readmore"]) && trim($blocks_item_v["readmore"]) != "") {
                $content[] = $blocks_item_v["readmore"];
            }
            if (isset($blocks_item_v["extratext"]) && trim($blocks_item_v["extratext"]) != "") {
                $content[] = $blocks_item_v["extratext"];
            }
            if (isset($blocks_item_v["extratextarea"]) && trim($blocks_item_v["extratextarea"]) != "") {
                $content[] = $blocks_item_v["extratextarea"];
            }
        }
        return implode(" ", $content);
    }

    public function view()
    {
        $db = Database::connection();
        $layout_options = array(
            'orange' => "Orange box",
            'testimonial' => "Testimonials",
            'projects' => "Related Projects"
        );
        $this->set("layout_options", $layout_options);
        $blocks = array();
        $blocks["extraselect_options"] = array(
            '' => "-- " . t("None") . " --",
            'foo' => "Foo",
            'bar' => "Bar"
        );
        $blocks_items = $db->fetchAll('SELECT * FROM btBlocksBlocksEntries WHERE bID = ? ORDER BY sortOrder', array($this->bID));
        foreach ($blocks_items as $blocks_item_k => &$blocks_item_v) {
            if (isset($blocks_item_v['background']) && trim($blocks_item_v['background']) != "" && ($f = File::getByID($blocks_item_v['background'])) && is_object($f)) {
                $blocks_item_v['background'] = $f;
            } else {
                $blocks_item_v['background'] = false;
            }
            if (!isset($blocks_item_v["extraboolean"]) || trim($blocks_item_v["extraboolean"]) == "") {
                $blocks_item_v["extraboolean"] = '0';
            }
            if (isset($blocks_item_v['extraimage']) && trim($blocks_item_v['extraimage']) != "" && ($f = File::getByID($blocks_item_v['extraimage'])) && is_object($f)) {
                $blocks_item_v['extraimage'] = $f;
            } else {
                $blocks_item_v['extraimage'] = false;
            }
        }
        $this->set('blocks_items', $blocks_items);
        $this->set('blocks', $blocks);
    }

    public function delete()
    {
        $db = Database::connection();
        $db->delete('btBlocksBlocksEntries', array('bID' => $this->bID));
        parent::delete();
    }

    public function duplicate($newBID)
    {
        $db = Database::connection();
        $blocks_items = $db->fetchAll('SELECT * FROM btBlocksBlocksEntries WHERE bID = ? ORDER BY sortOrder', array($this->bID));
        foreach ($blocks_items as $blocks_item) {
            unset($blocks_item['id']);
            $blocks_item['bID'] = $newBID;
            $db->insert('btBlocksBlocksEntries', $blocks_item);
        }
        parent::duplicate($newBID);
    }

    public function add()
    {
        $this->addEdit();
        $blocks = $this->get('blocks');
        $this->set('blocks_items', array());
        
        $this->set('blocks', $blocks);
    }

    public function edit()
    {
        $db = Database::connection();
        $this->addEdit();
        $blocks = $this->get('blocks');
        $blocks_items = $db->fetchAll('SELECT * FROM btBlocksBlocksEntries WHERE bID = ? ORDER BY sortOrder', array($this->bID));
        foreach ($blocks_items as &$blocks_item) {
            if (!File::getByID($blocks_item['background'])) {
                unset($blocks_item['background']);
            }
        }
        foreach ($blocks_items as &$blocks_item) {
            if (!File::getByID($blocks_item['extraimage'])) {
                unset($blocks_item['extraimage']);
            }
        }
        $this->set('blocks', $blocks);
        $this->set('blocks_items', $blocks_items);
    }

    protected function addEdit()
    {
        $this->set("layout_options", array(
                'orange' => "Orange box",
                'testimonial' => "Testimonials",
                'projects' => "Related Projects"
            )
        );
        $blocks = array();
        $blocks['extraselect_options'] = array(
            '' => "-- " . t("None") . " --",
            'foo' => "Foo",
            'bar' => "Bar"
        );
        $this->set('blocks', $blocks);
        $this->set('identifier', new \Concrete\Core\Utility\Service\Identifier());
        $al = \Concrete\Core\Asset\AssetList::getInstance();
        $al->register('css', 'repeatable-ft.form', 'blocks/blocks/css_form/repeatable-ft.form.css', array(), $this->pkg);
        $al->register('javascript', 'handlebars', 'blocks/blocks/js_form/handlebars-v4.0.4.js', array(), $this->pkg);
        $al->register('javascript', 'handlebars-helpers', 'blocks/blocks/js_form/handlebars-helpers.js', array(), $this->pkg);
        $this->requireAsset('core/sitemap');
        $this->requireAsset('css', 'repeatable-ft.form');
        $this->requireAsset('javascript', 'handlebars');
        $this->requireAsset('javascript', 'handlebars-helpers');
        $this->requireAsset('core/file-manager');
        $this->set('btFieldsRequired', $this->btFieldsRequired);
    }

    public function save($args)
    {
        $db = Database::connection();
        $rows = $db->fetchAll('SELECT * FROM btBlocksBlocksEntries WHERE bID = ? ORDER BY sortOrder', array($this->bID));
        $blocks_items = isset($args['blocks']) && is_array($args['blocks']) ? $args['blocks'] : array();
        $queries = array();
        if (!empty($blocks_items)) {
            $i = 0;
            foreach ($blocks_items as $blocks_item) {
                $data = array(
                    'sortOrder' => $i + 1,
                );
                if (isset($blocks_item['title']) && trim($blocks_item['title']) != '') {
                    $data['title'] = trim($blocks_item['title']);
                } else {
                    $data['title'] = null;
                }
                if (isset($blocks_item['details']) && trim($blocks_item['details']) != '') {
                    $data['details'] = trim($blocks_item['details']);
                } else {
                    $data['details'] = null;
                }
                if (isset($blocks_item['pagelink']) && trim($blocks_item['pagelink']) != '' && (($blocks_item['pagelink_c'] = Page::getByID($blocks_item['pagelink'])) && !$blocks_item['pagelink_c']->error)) {
                    $data['pagelink'] = trim($blocks_item['pagelink']);
                } else {
                    $data['pagelink'] = null;
                }
                if (isset($blocks_item['external']) && trim($blocks_item['external']) != '') {
                    $data['external'] = trim($blocks_item['external']);
                } else {
                    $data['external'] = null;
                }
                if (isset($blocks_item['readmore']) && trim($blocks_item['readmore']) != '') {
                    $data['readmore'] = trim($blocks_item['readmore']);
                } else {
                    $data['readmore'] = null;
                }
                if (isset($blocks_item['background']) && trim($blocks_item['background']) != '') {
                    $data['background'] = trim($blocks_item['background']);
                } else {
                    $data['background'] = null;
                }
                if (isset($blocks_item['extratext']) && trim($blocks_item['extratext']) != '') {
                    $data['extratext'] = trim($blocks_item['extratext']);
                } else {
                    $data['extratext'] = null;
                }
                if (isset($blocks_item['extratextarea']) && trim($blocks_item['extratextarea']) != '') {
                    $data['extratextarea'] = trim($blocks_item['extratextarea']);
                } else {
                    $data['extratextarea'] = null;
                }
                if (isset($blocks_item['extraurl']) && trim($blocks_item['extraurl']) != '') {
                    $data['extraurl'] = trim($blocks_item['extraurl']);
                } else {
                    $data['extraurl'] = null;
                }
                if (isset($blocks_item['extraselect']) && trim($blocks_item['extraselect']) != '') {
                    $data['extraselect'] = trim($blocks_item['extraselect']);
                } else {
                    $data['extraselect'] = null;
                }
                if (isset($blocks_item['extraboolean']) && trim($blocks_item['extraboolean']) != '' && in_array($blocks_item['extraboolean'], array(0, 1))) {
                    $data['extraboolean'] = $blocks_item['extraboolean'];
                } else {
                    $data['extraboolean'] = null;
                }
                if (isset($blocks_item['extraimage']) && trim($blocks_item['extraimage']) != '') {
                    $data['extraimage'] = trim($blocks_item['extraimage']);
                } else {
                    $data['extraimage'] = null;
                }
                if (isset($rows[$i])) {
                    $queries['update'][$rows[$i]['id']] = $data;
                    unset($rows[$i]);
                } else {
                    $data['bID'] = $this->bID;
                    $queries['insert'][] = $data;
                }
                $i++;
            }
        }
        if (!empty($rows)) {
            foreach ($rows as $row) {
                $queries['delete'][] = $row['id'];
            }
        }
        if (!empty($queries)) {
            foreach ($queries as $type => $values) {
                if (!empty($values)) {
                    switch ($type) {
                        case 'update':
                            foreach ($values as $id => $data) {
                                $db->update('btBlocksBlocksEntries', $data, array('id' => $id));
                            }
                            break;
                        case 'insert':
                            foreach ($values as $data) {
                                $db->insert('btBlocksBlocksEntries', $data);
                            }
                            break;
                        case 'delete':
                            foreach ($values as $value) {
                                $db->delete('btBlocksBlocksEntries', array('id' => $value));
                            }
                            break;
                    }
                }
            }
        }
        parent::save($args);
    }

    public function validate($args)
    {
        $e = Core::make("helper/validation/error");
        if ((in_array("layout", $this->btFieldsRequired) && (!isset($args["layout"]) || trim($args["layout"]) == "")) || (isset($args["layout"]) && trim($args["layout"]) != "" && !in_array($args["layout"], array("orange", "testimonial", "projects")))) {
            $e->add(t("The %s field has an invalid value.", t("Layout")));
        }
        $blocksEntriesMin = 0;
        $blocksEntriesMax = 0;
        $blocksEntriesErrors = 0;
        $blocks = array();
        if (isset($args['blocks']) && is_array($args['blocks']) && !empty($args['blocks'])) {
            if ($blocksEntriesMin >= 1 && count($args['blocks']) < $blocksEntriesMin) {
                $e->add(t("The %s field requires at least %s entries, %s entered.", t("Blocks"), $blocksEntriesMin, count($args['blocks'])));
                $blocksEntriesErrors++;
            }
            if ($blocksEntriesMax >= 1 && count($args['blocks']) > $blocksEntriesMax) {
                $e->add(t("The %s field is set to a maximum of %s entries, %s entered.", t("Blocks"), $blocksEntriesMax, count($args['blocks'])));
                $blocksEntriesErrors++;
            }
            if ($blocksEntriesErrors == 0) {
                foreach ($args['blocks'] as $blocks_k => $blocks_v) {
                    if (is_array($blocks_v)) {
                        if (in_array("title", $this->btFieldsRequired['blocks']) && (!isset($blocks_v['title']) || trim($blocks_v['title']) == "")) {
                            $e->add(t("The %s field is required (%s, row #%s).", t("Title"), t("Blocks"), $blocks_k));
                        }
                        if (in_array("details", $this->btFieldsRequired['blocks']) && (!isset($blocks_v['details']) || trim($blocks_v['details']) == "")) {
                            $e->add(t("The %s field is required (%s, row #%s).", t("Details"), t("Blocks"), $blocks_k));
                        }
                        if ((in_array("pagelink", $this->btFieldsRequired['blocks']) || (isset($blocks_v['pagelink']) && trim($blocks_v['pagelink']) != '0')) && (trim($blocks_v['pagelink']) == "" || (($page = Page::getByID($blocks_v['pagelink'])) && $page->error !== false))) {
                            $e->add(t("The %s field is required (%s, row #%s).", t("Page Link"), t("Blocks"), $blocks_k));
                        }
                        if (((!in_array("external", $this->btFieldsRequired['blocks']) && isset($blocks_v['external']) && trim($blocks_v['external']) != "") || (in_array("external", $this->btFieldsRequired['blocks']))) && (!filter_var($blocks_v['external'], FILTER_VALIDATE_URL))) {
                            $e->add(t("The %s field does not have a valid URL (%s, row #%s).", t("External Link"), t("Blocks"), $blocks_k));
                        }
                        if (in_array("readmore", $this->btFieldsRequired['blocks']) && (!isset($blocks_v['readmore']) || trim($blocks_v['readmore']) == "")) {
                            $e->add(t("The %s field is required (%s, row #%s).", t("Read More Text"), t("Blocks"), $blocks_k));
                        }
                        if (in_array("background", $this->btFieldsRequired['blocks']) && (!isset($blocks_v['background']) || trim($blocks_v['background']) == "" || !is_object(File::getByID($blocks_v['background'])))) {
                            $e->add(t("The %s field is required (%s, row #%s).", t("Background"), t("Blocks"), $blocks_k));
                        }
                        if (in_array("extratext", $this->btFieldsRequired['blocks']) && (!isset($blocks_v['extratext']) || trim($blocks_v['extratext']) == "")) {
                            $e->add(t("The %s field is required (%s, row #%s).", t("Extra Text"), t("Blocks"), $blocks_k));
                        }
                        if (in_array("extratextarea", $this->btFieldsRequired['blocks']) && (!isset($blocks_v['extratextarea']) || trim($blocks_v['extratextarea']) == "")) {
                            $e->add(t("The %s field is required (%s, row #%s).", t("Extra Textarea"), t("Blocks"), $blocks_k));
                        }
                        if (((!in_array("extraurl", $this->btFieldsRequired['blocks']) && isset($blocks_v['extraurl']) && trim($blocks_v['extraurl']) != "") || (in_array("extraurl", $this->btFieldsRequired['blocks']))) && (!filter_var($blocks_v['extraurl'], FILTER_VALIDATE_URL))) {
                            $e->add(t("The %s field does not have a valid URL (%s, row #%s).", t("Extra URL"), t("Blocks"), $blocks_k));
                        }
                        if ((in_array("extraselect", $this->btFieldsRequired['blocks']) && (!isset($blocks_v['extraselect']) || trim($blocks_v['extraselect']) == "")) || (isset($blocks_v['extraselect']) && trim($blocks_v['extraselect']) != "" && !in_array($blocks_v['extraselect'], array("foo", "bar")))) {
                            $e->add(t("The %s field has an invalid value (%s, row #%s).", t("Extra Select"), t("Blocks"), $blocks_k));
                        }
                        if (in_array("extraboolean", $this->btFieldsRequired['blocks']) && (!isset($blocks_v['extraboolean']) || trim($blocks_v['extraboolean']) == "" || !in_array($blocks_v['extraboolean'], array(0, 1)))) {
                            $e->add(t("The %s field is required.", t("Extra Boolean"), t("Blocks"), $blocks_k));
                        }
                        if (in_array("extraimage", $this->btFieldsRequired['blocks']) && (!isset($blocks_v['extraimage']) || trim($blocks_v['extraimage']) == "" || !is_object(File::getByID($blocks_v['extraimage'])))) {
                            $e->add(t("The %s field is required (%s, row #%s).", t("Extra Image"), t("Blocks"), $blocks_k));
                        }
                    } else {
                        $e->add(t("The values for the %s field, row #%s, are incomplete.", t('Blocks'), $blocks_k));
                    }
                }
            }
        } else {
            if ($blocksEntriesMin >= 1) {
                $e->add(t("The %s field requires at least %s entries, none entered.", t("Blocks"), $blocksEntriesMin));
            }
        }
        return $e;
    }

    public function composer()
    {
        $al = \Concrete\Core\Asset\AssetList::getInstance();
        $al->register('javascript', 'auto-js-blocks', 'blocks/blocks/auto.js', array(), $this->pkg);
        $this->requireAsset('javascript', 'auto-js-blocks');
        $this->edit();
    }
}