<?php  defined("C5_EXECUTE") or die("Access Denied."); ?>

<?php  $unique_tabs_id = Core::make('helper/validation/identifier')->getString(18);
$tabs = array(
    array('form-basics-' . $unique_tabs_id, t('Basics'), true),
    array('form-steps_items-' . $unique_tabs_id, t('Steps'))
);
echo Core::make('helper/concrete/ui')->tabs($tabs); ?>

<div class="ccm-tab-content" id="ccm-tab-content-form-basics-<?php  echo $unique_tabs_id; ?>">
    <div class="form-group">
    <?php  echo $form->label('heading', t("Heading")); ?>
    <?php  echo isset($btFieldsRequired) && in_array('heading', $btFieldsRequired) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <?php  echo $form->text($view->field('heading'), $heading, array (
  'maxlength' => 255,
)); ?>
</div>
</div>

<div class="ccm-tab-content" id="ccm-tab-content-form-steps_items-<?php  echo $unique_tabs_id; ?>">
    <script type="text/javascript">
    var CCM_EDITOR_SECURITY_TOKEN = "<?php  echo Core::make('helper/validation/token')->generate('editor')?>";
</script>
<?php  $repeatable_container_id = 'btSteps-steps-container-' . Core::make('helper/validation/identifier')->getString(18); ?>
    <div id="<?php  echo $repeatable_container_id; ?>">
        <div class="sortable-items-wrapper">
            <a href="#" class="btn btn-primary add-entry">
                <?php  echo t('Add Entry'); ?>
            </a>

            <div class="sortable-items" data-attr-content="<?php  echo htmlspecialchars(
                json_encode(
                    array(
                        'items' => $steps_items,
                        'order' => array_keys($steps_items),
                    )
                )
            ); ?>">
            </div>

            <a href="#" class="btn btn-primary add-entry add-entry-last">
                <?php  echo t('Add Entry'); ?>
            </a>
        </div>

        <script class="repeatableTemplate" type="text/x-handlebars-template">
            <div class="sortable-item" data-id="{{id}}">
                <div class="sortable-item-title">
                    <span class="sortable-item-title-default">
                        <?php  echo t('Steps') . ' ' . t("row") . ' <span>#{{id}}</span>'; ?>
                    </span>
                    <span class="sortable-item-title-generated"></span>
                </div>

                <div class="sortable-item-inner">            <div class="form-group">
    <label for="<?php  echo $view->field('steps'); ?>[{{id}}][title]" class="control-label"><?php  echo t("Title"); ?></label>
    <?php  echo isset($btFieldsRequired['steps']) && in_array('title', $btFieldsRequired['steps']) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <input name="<?php  echo $view->field('steps'); ?>[{{id}}][title]" id="<?php  echo $view->field('steps'); ?>[{{id}}][title]" class="form-control title-me" type="text" value="{{ title }}" maxlength="255" />
</div>            <div class="form-group">
    <label for="<?php  echo $view->field('steps'); ?>[{{id}}][icon]" class="control-label"><?php  echo t("Icon"); ?></label>
    <?php  echo isset($btFieldsRequired['steps']) && in_array('icon', $btFieldsRequired['steps']) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <div class="font-awesome-group">
        <?php  $stepsIcon_options = $steps['icon_options']; ?>
                    <select name="<?php  echo $view->field('steps'); ?>[{{id}}][icon]" id="<?php  echo $view->field('steps'); ?>[{{id}}][icon]" class="form-control font-awesome-previewed">{{#select icon}}<?php  foreach ($stepsIcon_options as $k => $v) {
                        echo "<option value='" . $k . "'>" . $v . "</option>";
                     } ?>{{/select}}</select>
        <i data-preview="icon" class=""></i>
    </div>
</div>            <div class="form-group">
    <label for="<?php  echo $view->field('steps'); ?>[{{id}}][link]" class="control-label"><?php  echo t("Link"); ?></label>
    <?php  echo isset($btFieldsRequired['steps']) && in_array('link', $btFieldsRequired['steps']) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <div data-page-selector="{{token}}" data-input-name="<?php  echo $view->field('steps'); ?>[{{id}}][link]" data-cID="{{link}}" class="link-ft"></div>
</div></div>

                <span class="sortable-item-collapse-toggle"></span>

                <a href="#" class="sortable-item-delete" data-attr-confirm-text="<?php  echo t('Are you sure'); ?>">
                    <i class="fa fa-times"></i>
                </a>

                <div class="sortable-item-handle">
                    <i class="fa fa-sort"></i>
                </div>
            </div>
        </script>
    </div>

<script type="text/javascript">
    Concrete.event.publish('btSteps.steps.edit.open', {id: '<?php  echo $repeatable_container_id; ?>'});
    $.each($('#<?php  echo $repeatable_container_id; ?> input[type="text"].title-me'), function () {
        $(this).trigger('keyup');
    });
</script>
</div>