<?php defined("C5_EXECUTE") or die("Access Denied."); ?>

<?php if(!empty($steps_items)): ?>
	<?php $columns = count($steps_items); ?>
	<div id="steps">
		<?php if(isset($heading) && trim($heading) != ""): ?>
			<div class="row">
				<div class="col-xs-12">
					<h2 class="page-title"><?=h($heading); ?></h2>
				</div>
			</div>
		<?php endif; ?>
		<div class="arrows row">
			<?php $i = 0; foreach($steps_items as $steps_item_key => $steps_item): ?>
				<?php
					$link = false;
					if(!empty($steps_item["link"]) && ($steps_item["link_c"] = Page::getByID($steps_item["link"])) && !$steps_item["link_c"]->error && !$steps_item["link_c"]->isInTrash()) $link = $steps_item["link_c"]->getCollectionLink();
				?>
				<div class="col-sm-2<?=($columns % 5 == 0 && $i % 5 == 0 ? ' col-sm-offset-1' : '');?>">
				<?php if($link !== false): ?>
					<a class="step" href="<?=$link;?>">
				<?php else: ?>
					<span class="step">
				<?php endif; ?>
				<?php if(isset($steps_item["icon"]) && trim($steps_item["icon"]) != ""): ?>
					<i class="fa fa-fw <?php echo $steps_item["icon"]; ?>"></i>
				<?php endif; ?>
				<?php if(isset($steps_item["title"]) && trim($steps_item["title"]) != ""): ?>
					<span class="title"><?php echo h($steps_item["title"]); ?></span>
				<?php endif; ?>
				<?php if($link !== false): ?>
					</a>
				<?php else: ?>
					</span>
				<?php endif; ?>
				</div>
			<?php $i++; endforeach; ?>
		</div>
	</div>
<?php endif; ?>