<?php defined("C5_EXECUTE") or die("Access Denied."); ?>
<?php if(!empty($link_items)): ?>
	<?php foreach($link_items as $link_item_key => $link_item): ?>
		<?php if(isset($link_item["url"]) && trim($link_item["url"]) != ""): ?>
			<li>
				<a href="<?=$link_item['url'];?>">
					<?php if(isset($link_item["icon"]) && trim($link_item["icon"]) != ""): ?>
						<i class="fa <?=$link_item["icon"]; ?>"></i>
					<?php endif; ?>
					<?php if(isset($link_item['url_text']) && trim($link_item['url_text']) != ''): ?><em><?=$link_item['url_text'];?></em><?php endif; ?>
				</a>
			</li>
		<?php endif; ?>
	<?php endforeach; ?>
<?php endif; ?>