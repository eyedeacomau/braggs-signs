<?php  defined("C5_EXECUTE") or die("Access Denied."); ?>

<script type="text/javascript">
    var CCM_EDITOR_SECURITY_TOKEN = "<?php  echo Core::make('helper/validation/token')->generate('editor')?>";
</script>
<?php  $repeatable_container_id = 'btHeaderInfo-link-container-' . Core::make('helper/validation/identifier')->getString(18); ?>
    <div id="<?php  echo $repeatable_container_id; ?>">
        <div class="sortable-items-wrapper">
            <a href="#" class="btn btn-primary add-entry">
                <?php  echo t('Add Entry'); ?>
            </a>

            <div class="sortable-items" data-attr-content="<?php  echo htmlspecialchars(
                json_encode(
                    array(
                        'items' => $link_items,
                        'order' => array_keys($link_items),
                    )
                )
            ); ?>">
            </div>

            <a href="#" class="btn btn-primary add-entry add-entry-last">
                <?php  echo t('Add Entry'); ?>
            </a>
        </div>

        <script class="repeatableTemplate" type="text/x-handlebars-template">
            <div class="sortable-item" data-id="{{id}}">
                <div class="sortable-item-title">
                    <span class="sortable-item-title-default">
                        <?php  echo t('Link') . ' ' . t("row") . ' <span>#{{id}}</span>'; ?>
                    </span>
                    <span class="sortable-item-title-generated"></span>
                </div>

                <div class="sortable-item-inner">            <div class="form-group">
    <label for="<?php  echo $view->field('link'); ?>[{{id}}][icon]" class="control-label"><?php  echo t("Icon"); ?></label>
    <?php  echo isset($btFieldsRequired['link']) && in_array('icon', $btFieldsRequired['link']) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <div class="font-awesome-group">
        <?php  $linkIcon_options = $link['icon_options']; ?>
                    <select name="<?php  echo $view->field('link'); ?>[{{id}}][icon]" id="<?php  echo $view->field('link'); ?>[{{id}}][icon]" class="form-control font-awesome-previewed">{{#select icon}}<?php  foreach ($linkIcon_options as $k => $v) {
                        echo "<option value='" . $k . "'>" . $v . "</option>";
                     } ?>{{/select}}</select>
        <i data-preview="icon" class=""></i>
    </div>
</div>            <div class="form-group">
    <label for="<?php  echo $view->field('link'); ?>[{{id}}][url]" class="control-label"><?php  echo t("URL"); ?></label>
    <?php  echo isset($btFieldsRequired['link']) && in_array('url', $btFieldsRequired['link']) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <input name="<?php  echo $view->field('link'); ?>[{{id}}][url]" id="<?php  echo $view->field('link'); ?>[{{id}}][url]" class="form-control" type="text" value="{{ url }}" />
</div>

<div class="form-group">
    <label for="<?php  echo $view->field('link'); ?>[{{id}}][url_text]" class="control-label"><?php  echo t("URL") . " " . t('Text'); ?></label>
    <input name="<?php  echo $view->field('link'); ?>[{{id}}][url_text]" id="<?php  echo $view->field('link'); ?>[{{id}}][url_text]" class="form-control" type="text" value="{{ url_text }}" />
</div></div>

                <span class="sortable-item-collapse-toggle"></span>

                <a href="#" class="sortable-item-delete" data-attr-confirm-text="<?php  echo t('Are you sure'); ?>">
                    <i class="fa fa-times"></i>
                </a>

                <div class="sortable-item-handle">
                    <i class="fa fa-sort"></i>
                </div>
            </div>
        </script>
    </div>

<script type="text/javascript">
    Concrete.event.publish('btHeaderInfo.link.edit.open', {id: '<?php  echo $repeatable_container_id; ?>'});
    $.each($('#<?php  echo $repeatable_container_id; ?> input[type="text"].title-me'), function () {
        $(this).trigger('keyup');
    });
</script>