<?php  defined('C5_EXECUTE') or die("Access Denied."); ?>
<div class="black-wrapper">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<<?php echo $formatting;?> class="page-title"><?php echo h($title)?></<?php echo $formatting;?>>
			</div>
		</div>
	</div>
</div>
