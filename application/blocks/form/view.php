<?php
/************************************************************
 * DESIGNERS: SCROLL DOWN! (IGNORE ALL THIS STUFF AT THE TOP)
 ************************************************************/
defined('C5_EXECUTE') or die("Access Denied.");
use \Concrete\Block\Form\MiniSurvey;

$survey = $controller;
$miniSurvey = new MiniSurvey($b);
$miniSurvey->frontEndMode = true;

//Clean up variables from controller so html is easier to work with...
$bID = intval($bID);
$qsID = intval($survey->questionSetId);
$formAction = $view->action('submit_form').'#formblock'.$bID;

$questionsRS = $miniSurvey->loadQuestions($qsID, $bID);
$questions = array();
while ($questionRow = $questionsRS->fetchRow()) {
	$question = $questionRow;
	$question['input'] = $miniSurvey->loadInputType($questionRow, false);
	//Make type names common-sensical
	if($questionRow['inputType'] == 'text') {
		$question['type'] = 'textarea';
	} else if($questionRow['inputType'] == 'field') {
		$question['type'] = 'text';
	} else {
		$question['type'] = $questionRow['inputType'];
	}

    $question['labelFor'] = 'for="Question' . $questionRow['msqID'] . '"';
	//Remove hardcoded style on textareas
	if($question['type'] == 'textarea') {
		$question['input'] = str_replace('style="width:95%"', '', $question['input']);
	}
	if($question['type'] == 'select') {
		$question['input'] = str_replace('selected="selected"', 'selected disabled', $question['input']);
		$question['input'] = str_replace('----</option><option >*', '----</option><optgroup label="', $question['input']);
		$question['input'] = str_replace('*</option>', '">', $question['input']);
		$question['input'] = str_replace('<option >*', '</optgroup><optgroup label="', $question['input']);
		if(stripos($question['input'],'optgroup') !== false) $question['input'] = str_replace('</option></select>', '</option></optgroup></select>', $question['input']);
		$question['input'] = str_replace('----', 'Choose a '.strtolower($question['question']), $question['input']);
		$question['input'] = str_replace('form-control', 'form-control selectpicker', $question['input']);
	}
	$questions[] = $question;
}

//Prep thank-you message
$success = (\Request::request('surveySuccess') && \Request::request('qsid') == intval($qsID));
$thanksMsg = $survey->thankyouMsg;

//Collate all errors and put them into divs
$errorHeader = isset($formResponse) ? $formResponse : null;
$errors = isset($errors) && is_array($errors) ? $errors : array();
if(isset($invalidIP) && $invalidIP) {
	$errors[] = $invalidIP;
}
$errorDivs = '';
foreach ($errors as $error) {
	$errorDivs .= '<div class="error">'.$error."</div>\n"; //It's okay for this one thing to have the html here -- it can be identified in CSS via parent wrapper div (e.g. '.formblock .error')
}

//Prep captcha
$surveyBlockInfo = $miniSurvey->getMiniSurveyBlockInfoByQuestionId($qsID, $bID);
$captcha = $surveyBlockInfo['displayCaptcha'] ? Loader::helper('validation/captcha') : false;

/******************************************************************************
* DESIGNERS: CUSTOMIZE THE FORM HTML STARTING HERE...
*/?>


	<div id="formblock<?=$bID; ?>" class="ccm-block-type-form col-xs-12 col-sm-8">
		<form enctype="multipart/form-data" class="form-stacked miniSurveyView" id="miniSurveyView<?=$bID; ?>" method="post" action="<?=$formAction ?>">
			<?php if($success): ?>
				<div class="alert alert-success">
					<?=h($thanksMsg); ?>
				</div>
			<?php elseif($errors): ?>
				<div class="alert alert-danger">
					<?=$errorHeader; ?>
					<?=$errorDivs; /* each error wrapped in <div class="error">...</div> */ ?>
				</div>
			<?php endif; ?>
			<div class="fields">
				<?php
					$columns = 2;
					$per_column = ceil(count($questions) / $columns);
				?>
				<div class="row">
				<?php $i = 0; foreach ($questions as $question): ?>
					<?php if($i % $per_column == 0): ?>
						<?php if($i > 0): ?></div><?php endif; // closing columns ?>
						<?php if($columns == 3): ?>
							<?php if($i < 6): ?>
								<div class="col-sm-3 col-md-4">
							<?php else: ?>
								<div class="col-sm-6 col-md-4">
							<?php endif; ?>
						<?php else: ?>
							<div class="col-sm-6">
						<?php endif; ?>
					<?php endif; ?>
						<div class="form-group field field-<?=$question['type']; ?> <?=isset($errorDetails[$question['msqID']]) ? 'has-error' : ''?>">
							<label class="control-label" <?=$question['labelFor']; ?>>
								<?=$question['question']; ?>
								<?php if($question['required']): ?>
									<span class="required"><i class="fa fa-asterisk" aria-hidden="true"></i></span>
								<?php endif; ?>
							</label>
							<?=$question['input']; ?>
						</div>
				<?php $i++; endforeach; ?></div><?php // This extra div here is to close the last column that doesn't go into the next foreach loop ?>
				</div>
			</div><!-- .fields -->
			<?php if($captcha): ?>
				<div class="form-group captcha">
					<?php
					$captchaLabel = $captcha->label();
					if(!empty($captchaLabel)) {
						?>
						<label class="control-label"><?=$captchaLabel; ?></label>
						<?php
					}
					?>
					<div><?php $captcha->display(); ?></div>
					<div><?php $captcha->showInput(); ?></div>
				</div>
			<?php endif; ?>

			<div class="form-actions row">
				<?php if($columns == 3): ?>
					<div class="col-md-4 col-md-offset-8 col-sm-6 col-sm-offset-6 col-xs-12 col-xs-offset-0">
				<?php else: ?>
					<div class="col-xs-12 col-xs-offset-0 col-sm-6 col-sm-offset-6">
				<?php endif; ?>
					<input type="submit" name="Submit" class="btn btn-primary" value="<?=h(t($survey->submitText)); ?>" />
				</div>
			</div>

			<input name="qsID" type="hidden" value="<?=$qsID; ?>" />
			<input name="pURI" type="hidden" value="<?=isset($pURI) ? $pURI : ''; ?>" />
		</form>
	</div><!-- .formblock -->
