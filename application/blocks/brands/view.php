<?php defined("C5_EXECUTE") or die("Access Denied."); ?>
<div id="brands" class="carousel"<?=($background ? ' style="background-image: url('.$background->getURL().');"' : '');?>>
    <div class="container">
        <?php if(isset($heading) && trim($heading) != ""): ?>
            <div class="row">
                <div class="col-xs-12">
                    <h2 class="page-title"><?=h($heading); ?></h2>
                </div>
            </div>
        <?php endif; ?>
        <?php if(!empty($brands_items)): ?>
            <div class="row">
                <div class="col-xs-2 col-sm-1 text-right">
                    <a href="#prev" class="nav prev"><i class="fa fa-long-arrow-left"></i></a>
                </div>
                <div class="col-xs-8 col-sm-10">
                    <div class="owl-carousel">
                        <?php foreach($brands_items as $brands_item_key => $brands_item): ?>
                            <?php if($brands_item["brand"]): ?>
                                <?php if(trim($brands_item['brand_url']) != ''): ?>
                                    <a target="_blank" href="<?=$brands_item['brand_url'];?>" class="item">
                                <?php else: ?>
                                    <div class="item">
                                <?php endif; ?>
                                    <img src="<?=$brands_item['brand']->getURL();?>" />
                                <?php if(trim($brands_item['brand_url']) != ''): ?>
                                    </a>
                                <?php else: ?>
                                    </div>
                                <?php endif; ?>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </div>
                </div>
                <div class="col-xs-2 col-sm-1">
                    <a href="#next" class="nav next"><i class="fa fa-long-arrow-right"></i></a>
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>
