<?php  namespace Application\Block\Brands;

defined("C5_EXECUTE") or die("Access Denied.");

use Concrete\Core\Block\BlockController;
use Core;
use File;
use Page;
use Database;

class Controller extends BlockController
{
    public $helpers = array('form');
    public $btFieldsRequired = array('brands', 'brands' => array());
    protected $btExportFileColumns = array('background');
    protected $btExportTables = array('btBrands', 'btBrandsBrandsEntries');
    protected $btTable = 'btBrands';
    protected $btInterfaceWidth = 400;
    protected $btInterfaceHeight = 500;
    protected $btIgnorePageThemeGridFrameworkContainer = true;
    protected $btCacheBlockRecord = true;
    protected $btCacheBlockOutput = true;
    protected $btCacheBlockOutputOnPost = true;
    protected $btCacheBlockOutputForRegisteredUsers = true;
    protected $btCacheBlockOutputLifetime = 0;
    protected $pkg = false;
    
    public function getBlockTypeDescription()
    {
        return t("");
    }

    public function getBlockTypeName()
    {
        return t("Brands");
    }

    public function getSearchableContent()
    {
        $content = array();
        $content[] = $this->heading;
        return implode(" ", $content);
    }

    public function view()
    {
        $db = Database::connection();
        
        if ($this->background && ($f = File::getByID($this->background)) && is_object($f)) {
            $this->set("background", $f);
        } else {
            $this->set("background", false);
        }
        $brands = array();
        $brands_items = $db->fetchAll('SELECT * FROM btBrandsBrandsEntries WHERE bID = ? ORDER BY sortOrder', array($this->bID));
        foreach ($brands_items as $brands_item_k => &$brands_item_v) {
            if (isset($brands_item_v['brand']) && trim($brands_item_v['brand']) != "" && ($f = File::getByID($brands_item_v['brand'])) && is_object($f)) {
                $brands_item_v['brand'] = $f;
            } else {
                $brands_item_v['brand'] = false;
            }
        }
        $this->set('brands_items', $brands_items);
        $this->set('brands', $brands);
    }

    public function delete()
    {
        $db = Database::connection();
        $db->delete('btBrandsBrandsEntries', array('bID' => $this->bID));
        parent::delete();
    }

    public function duplicate($newBID)
    {
        $db = Database::connection();
        $brands_items = $db->fetchAll('SELECT * FROM btBrandsBrandsEntries WHERE bID = ? ORDER BY sortOrder', array($this->bID));
        foreach ($brands_items as $brands_item) {
            unset($brands_item['id']);
            $brands_item['bID'] = $newBID;
            $db->insert('btBrandsBrandsEntries', $brands_item);
        }
        parent::duplicate($newBID);
    }

    public function add()
    {
        $this->addEdit();
        $brands = $this->get('brands');
        $this->set('brands_items', array());
        $this->set('brands', $brands);
    }

    public function edit()
    {
        $db = Database::connection();
        $this->addEdit();
        $brands = $this->get('brands');
        $brands_items = $db->fetchAll('SELECT * FROM btBrandsBrandsEntries WHERE bID = ? ORDER BY sortOrder', array($this->bID));
        foreach ($brands_items as &$brands_item) {
            if (!File::getByID($brands_item['brand'])) {
                unset($brands_item['brand']);
            }
        }
        $this->set('brands', $brands);
        $this->set('brands_items', $brands_items);
    }

    protected function addEdit()
    {
        $brands = array();
        $this->set('brands', $brands);
        $this->set('identifier', new \Concrete\Core\Utility\Service\Identifier());
        $al = \Concrete\Core\Asset\AssetList::getInstance();
        $al->register('css', 'repeatable-ft.form', 'blocks/brands/css_form/repeatable-ft.form.css', array(), $this->pkg);
        $al->register('javascript', 'handlebars', 'blocks/brands/js_form/handlebars-v4.0.4.js', array(), $this->pkg);
        $al->register('javascript', 'handlebars-helpers', 'blocks/brands/js_form/handlebars-helpers.js', array(), $this->pkg);
        $this->requireAsset('core/file-manager');
        $this->requireAsset('core/sitemap');
        $this->requireAsset('css', 'repeatable-ft.form');
        $this->requireAsset('javascript', 'handlebars');
        $this->requireAsset('javascript', 'handlebars-helpers');
        $this->set('btFieldsRequired', $this->btFieldsRequired);
    }

    public function save($args)
    {
        $db = Database::connection();
        $rows = $db->fetchAll('SELECT * FROM btBrandsBrandsEntries WHERE bID = ? ORDER BY sortOrder', array($this->bID));
        $brands_items = isset($args['brands']) && is_array($args['brands']) ? $args['brands'] : array();
        $queries = array();
        if (!empty($brands_items)) {
            $i = 0;
            foreach ($brands_items as $brands_item) {
                $data = array(
                    'sortOrder' => $i + 1,
                );
                if (isset($brands_item['brand']) && trim($brands_item['brand']) != '') {
                    $data['brand'] = trim($brands_item['brand']);
                } else {
                    $data['brand'] = null;
                }
if (isset($brands_item['brand_url']) && trim($brands_item['brand_url']) != '') {
                    $data['brand_url'] = trim($brands_item['brand_url']);
                } else {
                    $data['brand_url'] = null;
                }
                if (isset($rows[$i])) {
                    $queries['update'][$rows[$i]['id']] = $data;
                    unset($rows[$i]);
                } else {
                    $data['bID'] = $this->bID;
                    $queries['insert'][] = $data;
                }
                $i++;
            }
        }
        if (!empty($rows)) {
            foreach ($rows as $row) {
                $queries['delete'][] = $row['id'];
            }
        }
        if (!empty($queries)) {
            foreach ($queries as $type => $values) {
                if (!empty($values)) {
                    switch ($type) {
                        case 'update':
                            foreach ($values as $id => $data) {
                                $db->update('btBrandsBrandsEntries', $data, array('id' => $id));
                            }
                            break;
                        case 'insert':
                            foreach ($values as $data) {
                                $db->insert('btBrandsBrandsEntries', $data);
                            }
                            break;
                        case 'delete':
                            foreach ($values as $value) {
                                $db->delete('btBrandsBrandsEntries', array('id' => $value));
                            }
                            break;
                    }
                }
            }
        }
        parent::save($args);
    }

    public function validate($args)
    {
        $e = Core::make("helper/validation/error");
        if (in_array("heading", $this->btFieldsRequired) && (trim($args["heading"]) == "")) {
            $e->add(t("The %s field is required.", t("Heading")));
        }
        if (in_array("background", $this->btFieldsRequired) && (trim($args["background"]) == "" || !is_object(File::getByID($args["background"])))) {
            $e->add(t("The %s field is required.", t("Background")));
        }
        $brandsEntriesMin = 0;
        $brandsEntriesMax = 0;
        $brandsEntriesErrors = 0;
        $brands = array();
        if (isset($args['brands']) && is_array($args['brands']) && !empty($args['brands'])) {
            if ($brandsEntriesMin >= 1 && count($args['brands']) < $brandsEntriesMin) {
                $e->add(t("The %s field requires at least %s entries, %s entered.", t("Brands"), $brandsEntriesMin, count($args['brands'])));
                $brandsEntriesErrors++;
            }
            if ($brandsEntriesMax >= 1 && count($args['brands']) > $brandsEntriesMax) {
                $e->add(t("The %s field is set to a maximum of %s entries, %s entered.", t("Brands"), $brandsEntriesMax, count($args['brands'])));
                $brandsEntriesErrors++;
            }
            if ($brandsEntriesErrors == 0) {
                foreach ($args['brands'] as $brands_k => $brands_v) {
                    if (is_array($brands_v)) {
                        // if (in_array("brand", $this->btFieldsRequired['brands']) && (!isset($brands_v['brand']) || trim($brands_v['brand']) == "" || !is_object(File::getByID($brands_v['brand'])))) {
                        //     $e->add(t("The %s field is required (%s, row #%s).", t("Brand"), t("Brands"), $brands_k));
                        // }elseif (is_object(File::getByID($brands_v['brand'])) && (trim($brands_v['brand_url']) == "" || !filter_var($brands_v['brand_url'], FILTER_VALIDATE_URL))) {
                        //       $e->add(t("The %s URL field does not have a valid URL (%s, row #%s).", t("Brand"), t("Brands"), $brands_k));
                        // }
                    } else {
                        $e->add(t("The values for the %s field, row #%s, are incomplete.", t('Brands'), $brands_k));
                    }
                }
            }
        } else {
            if ($brandsEntriesMin >= 1) {
                $e->add(t("The %s field requires at least %s entries, none entered.", t("Brands"), $brandsEntriesMin));
            }
        }
        return $e;
    }

    public function composer()
    {
        $al = \Concrete\Core\Asset\AssetList::getInstance();
        $al->register('javascript', 'auto-js-brands', 'blocks/brands/auto.js', array(), $this->pkg);
        $this->requireAsset('javascript', 'auto-js-brands');
        $this->edit();
    }
}