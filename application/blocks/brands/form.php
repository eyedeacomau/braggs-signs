<?php  defined("C5_EXECUTE") or die("Access Denied."); ?>

<?php  $unique_tabs_id = Core::make('helper/validation/identifier')->getString(18);
$tabs = array(
    array('form-basics-' . $unique_tabs_id, t('Basics'), true),
    array('form-brands_items-' . $unique_tabs_id, t('Brands'))
);
echo Core::make('helper/concrete/ui')->tabs($tabs); ?>

<div class="ccm-tab-content" id="ccm-tab-content-form-basics-<?php  echo $unique_tabs_id; ?>">
    <div class="form-group">
    <?php  echo $form->label('heading', t("Heading")); ?>
    <?php  echo isset($btFieldsRequired) && in_array('heading', $btFieldsRequired) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <?php  echo $form->text($view->field('heading'), $heading, array (
  'maxlength' => 255,
)); ?>
</div><div class="form-group">
    <?php 
    if (isset($background) && $background > 0) {
        $background_o = File::getByID($background);
        if (!is_object($background_o)) {
            unset($background_o);
        }
    } ?>
    <?php  echo $form->label('background', t("Background")); ?>
    <?php  echo isset($btFieldsRequired) && in_array('background', $btFieldsRequired) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <?php  echo Core::make("helper/concrete/asset_library")->image('ccm-b-brands-background-' . Core::make('helper/validation/identifier')->getString(18), $view->field('background'), t("Choose Image"), $background_o); ?>
</div>
</div>

<div class="ccm-tab-content" id="ccm-tab-content-form-brands_items-<?php  echo $unique_tabs_id; ?>">
    <script type="text/javascript">
    var CCM_EDITOR_SECURITY_TOKEN = "<?php  echo Core::make('helper/validation/token')->generate('editor')?>";
</script>
<?php  $repeatable_container_id = 'btBrands-brands-container-' . Core::make('helper/validation/identifier')->getString(18); ?>
    <div id="<?php  echo $repeatable_container_id; ?>">
        <div class="sortable-items-wrapper">
            <a href="#" class="btn btn-primary add-entry">
                <?php  echo t('Add Entry'); ?>
            </a>

            <div class="sortable-items" data-attr-content="<?php  echo htmlspecialchars(
                json_encode(
                    array(
                        'items' => $brands_items,
                        'order' => array_keys($brands_items),
                    )
                )
            ); ?>">
            </div>

            <a href="#" class="btn btn-primary add-entry add-entry-last">
                <?php  echo t('Add Entry'); ?>
            </a>
        </div>

        <script class="repeatableTemplate" type="text/x-handlebars-template">
            <div class="sortable-item" data-id="{{id}}">
                <div class="sortable-item-title">
                    <span class="sortable-item-title-default">
                        <?php  echo t('Brands') . ' ' . t("row") . ' <span>#{{id}}</span>'; ?>
                    </span>
                    <span class="sortable-item-title-generated"></span>
                </div>

                <div class="sortable-item-inner">            <div class="form-group">
    <label for="<?php  echo $view->field('brands'); ?>[{{id}}][brand]" class="control-label"><?php  echo t("Brand"); ?></label>
    <?php  echo isset($btFieldsRequired['brands']) && in_array('brand', $btFieldsRequired['brands']) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <div data-file-selector-input-name="<?php  echo $view->field('brands'); ?>[{{id}}][brand]" class="ccm-file-selector ft-image-brand-file-selector" data-file-selector-f-id="{{ brand }}"></div>
</div>
<div class="form-group">
    <label for="<?php  echo $view->field('brands'); ?>[{{id}}][brand_url]" class="control-label"><?php  echo t("Brand") . " " . t("url"); ?></label>
    <?php  echo '<small class="required">' . t('Required') . '</small>'; ?>
    <input name="<?php  echo $view->field('brands'); ?>[{{id}}][brand_url]" id="<?php  echo $view->field('brands'); ?>[{{id}}][brand_url]" class="form-control" type="text" value="{{ brand_url }}" maxlength="255" />
</div></div>

                <span class="sortable-item-collapse-toggle"></span>

                <a href="#" class="sortable-item-delete" data-attr-confirm-text="<?php  echo t('Are you sure'); ?>">
                    <i class="fa fa-times"></i>
                </a>

                <div class="sortable-item-handle">
                    <i class="fa fa-sort"></i>
                </div>
            </div>
        </script>
    </div>

<script type="text/javascript">
    Concrete.event.publish('btBrands.brands.edit.open', {id: '<?php  echo $repeatable_container_id; ?>'});
    $.each($('#<?php  echo $repeatable_container_id; ?> input[type="text"].title-me'), function () {
        $(this).trigger('keyup');
    });
</script>
</div>